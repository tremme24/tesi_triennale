#include "bitmap.h"

/*NB
Quando passo un indice di blocco a BitMap_blockToIndex con num/8 ottengo indice in array bitmap con num%8
ottengo offset su char in array[num/8] di bitmap.
*/

//Inits a Bitmap struct
BitMap BitMap_init(int num_bits, char* entries){
	BitMap bitmap_temp = {0};
    bitmap_temp.entries = entries;
    bitmap_temp.num_bits = num_bits;
	
	return bitmap_temp;
}

// converts a block index to an index in the array,
// and a char that indicates the offset of the bit inside the array
BitMapEntryKey BitMap_blockToIndex(int num){
	int entry_num = num / 8; //index in array
	char bit_num = num % 8; // offset of bit inside the array 

	BitMapEntryKey to_ret = {0};
	to_ret.entry_num = entry_num;
	to_ret.bit_num = bit_num; 

	return to_ret;
}

// converts a bit to a linear index
int BitMap_indexToBlock(int entry, uint8_t bit_num){
	return entry*8 + bit_num;
}

// returns the index of the first bit having status "status"
// in the bitmap bmap, and starts looking from position start
int BitMap_get(BitMap* bmap, int start, int status){
	if(status != 0 && status != 1) return -1;
	unsigned int entry_bitmap_char = start/8;

	// while(idx <= (bmap->num_bits)/8){
	//     unsigned char block = bmap->entries[idx];
	//     if(status == 0 && block == 255) continue;
	//     else if(status == 1 && block == 0) continue;
	//     for(int i = 7; i >= 0; i--){
	//         int idx_bit = 8*idx + (7-i);
	//         if(idx_bit >= start && idx_bit < bmap->num_bits){
	//             char bit = (block >> i) % 2;
	//             if(bit == status){
	//                 return (7-i)+idx*8; //è perfetto
	//             }
	//         }
	//     } 
	//     idx++;
	// }

	unsigned int bitmap_entries = bmap->num_bits%8? (bmap->num_bits/8)+1 : (bmap->num_bits/8);

	while(entry_bitmap_char < bitmap_entries){

		unsigned int* to_check = (unsigned int*)(bmap->entries + entry_bitmap_char);

		unsigned int uint_idxs = (bitmap_entries - entry_bitmap_char)/4;
		unsigned int i = 0;
		for(; i < uint_idxs; i++){
			// printf("Entro dove non devo entrare 1 \n\n");
			unsigned int block_to_check_uint = to_check[i];
			if((status == 0 && block_to_check_uint == UINT_MAX) || (status == 1 && block_to_check_uint == 0)) continue;
			else break;
		}

		entry_bitmap_char += i*sizeof(unsigned int);
		unsigned char block_to_check_uchar = bmap->entries[entry_bitmap_char];
		if((status == 0 && block_to_check_uchar == UCHAR_MAX) || (status == 1 && block_to_check_uchar == 0)) {
			// printf("Entro dove devo entrare\n\n");
			// printf("Entry bitmap char = %d\n\n", entry_bitmap_char);
			// printf("Other value = %d\n\n", bitmap_entries);
			entry_bitmap_char++;
			continue;
		}
		else{
			for(int i = 7; i >= 0; i--){
				unsigned int idx_bit = BitMap_indexToBlock(entry_bitmap_char, (7-i));
				if(idx_bit >= start && idx_bit < bmap->num_bits){
					unsigned char bit = (block_to_check_uchar >> i) % 2;
					if(bit == status){
						return idx_bit; //è perfetto
					}
				}
			} 
			entry_bitmap_char++;
		}
	}
	return -1;
}

// sets the bit at index pos in bmap to status
int BitMap_set(BitMap* bmap, int pos, int status){
	if(pos >= bmap->num_bits || pos < 0) return -1;
	if(status != 0 && status != 1) return -1;

	BitMapEntryKey bitmapEntryKey = BitMap_blockToIndex(pos);

	unsigned char prev = bmap->entries[bitmapEntryKey.entry_num]; 
	unsigned char mask = 1 << (7 - bitmapEntryKey.bit_num);

	if (status) bmap->entries[bitmapEntryKey.entry_num] = prev | mask;
	if (!status) bmap->entries[bitmapEntryKey.entry_num] = prev & ~mask;

	return 0; // se riuscito 0
}

unsigned char BitMap_isBusy(BitMapEntryKey* entry, char* bitmap){
	return ((unsigned char)bitmap[entry->entry_num] >> (7 - entry->bit_num)) % 2;
}

void debugBmap(BitMap* bmap){
	int idx = 0;
	while(idx <= (bmap->num_bits/8)){
		unsigned char block = bmap->entries[idx];
		for(int i = 7; i >= 0; i--){
			int idx_bit = 8*idx + (7-i);
			if(idx_bit < bmap->num_bits){
				char bit = (block >> i) % 2;
				printf("Element num: %d = %u\n", (7-i)+idx*8, bit);
			}
		}
		idx++;
	}

}

void BeEvilWhenTestingBitMap(BitMap* bmap){
  
  printf("Testing set...\n\n");
  printf("BitMap_set(bmap,5,1):\n");
  int res = BitMap_set(bmap,5,1);
  if(res == -1){
	printf("Error in set1\n\n");
  }else{
	  debugBmap(bmap);
  }

  printf("\n\n");

  printf("BitMap_set(bmap,5,0):\n");
  res = BitMap_set(bmap,5,0);
  if(res == -1){
	printf("Error in set2\n\n");
  }else{
	  debugBmap(bmap);
  }

  printf("\n\n");

  printf("BitMap_set(bmap,5,48):\n");
  res = BitMap_set(bmap,5,48);
  if(res == -1){
	printf("Error in set3\n\n");
  }else{
	  debugBmap(bmap);
  }

  printf("\n\n");
  
  printf("BitMap_set(bmap,15,1):\n");
  res = BitMap_set(bmap,15,1);
  if(res == -1){
	printf("Error in set4\n\n");
  }else{
	  debugBmap(bmap);
  }

  printf("\n\n");
  printf("Testing get on BitMap...\n");

  printf("Index: %d \n",BitMap_get(bmap,0,1));
  printf("Index: %d \n",BitMap_get(bmap,8,0));
  printf("Index: %d \n",BitMap_get(bmap,14,1));
  printf("Index: %d \n",BitMap_get(bmap,18,1));

  printf("BitMap_set(bmap,177,1):\n");
  res = BitMap_set(bmap,177,1);
  if(res == -1){
	printf("Error in set4\n\n");
  }else{
	  debugBmap(bmap);
  }
  
  printf("\n\n");
  printf("Index: %d \n",BitMap_get(bmap,18,1));

  debugBmap(bmap);

  for(int i = 0; i < 100; i++){
		printf("BitMap_set(bmap,%d,1):\n", i);
		res = BitMap_set(bmap,i,1);
		if(res == -1){
			printf("Error in set1000\n\n");
		}
  }
  debugBmap(bmap);
  printf("BitMap_set(bmap,18,0):\n");
  res = BitMap_set(bmap,18,0);
	if(res == -1){
		printf("Error in set1000\n\n");
	}
	else{
		debugBmap(bmap);
	}

}
