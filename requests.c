#include "requests.h"

DiskDriver disk;
SimpleFS fs;
DirectoryHandle* globalDHS[N_THREADS];
FileHandle* globalFHS[N_THREADS][MAX_N_FILE_OPEN];
RequestDispatched requests_dispatched[N_THREADS];

void Requests_directoryHandlesInit(){
    for(int i = 0; i < N_THREADS; i++) globalDHS[i] = NULL;
}

void Requests_fileHandlesInit(){
    for(int i = 0; i < N_THREADS; i++) {
        for(int j = 0; j < MAX_N_FILE_OPEN; j++){
            globalFHS[i][j] = NULL;
        }
    }
}

void Requests_globalSemaphoresInit(){
    int res;
    for(int i = 0; i < N_THREADS; i++){
        requests_dispatched[i].ret_value = 0;
        res = sem_init(&(requests_dispatched[i].waiting_sem), 0, 0);
        if(res < 0){
            perror("Error while initializing the dispatching requests semaphores");
            exit(-1);
        }
    }
}

void Requests_addFileHandle(FileHandle* fileHandle, int id_thread, int fd){
    globalFHS[id_thread][fd] = fileHandle;
}

Queue Requests_queueRequestsInit(PfCbFree freefn){
    return queue_create(sizeof(Request*), MAX_N_REQUESTS, freefn);
}

Request* Requests_requestInit(int thread_creator, Parameters* parameters){

    Request* request = (Request*)malloc(sizeof(Request));
    if(!request){
        printf("Error while allocating request struct\n\n");
        return NULL;
    }

    request->parameters = parameters;
    request->thread_creator = thread_creator;

    return request;
}

Parameters* Requests_parametersInit(RequestType request_type, ...){
    Parameters* parameters = (Parameters*)malloc(sizeof(Parameters));
    if(!parameters){
        printf("Error while allocating  parameters struct\n\n");
        return NULL;
    }

    parameters->request_type = request_type;

    va_list ap;
    va_start(ap, request_type); 

    switch (request_type) {
        case MakeDir:
        case ChangeDir:
        case CreateFile:
        case OpenFile:
        case Remove:
            parameters->fd   = -1;
            parameters->name = va_arg(ap, char*);
            parameters->data = NULL;
            parameters->size = -1;
            break;
        case CloseFile:
            parameters->fd   = va_arg(ap, int);
            parameters->name = NULL;
            parameters->data = NULL;
            parameters->size = -1;
            break;
        case SeekFile:
            parameters->fd   = va_arg(ap, int);        
            parameters->name = NULL;
            parameters->data = NULL;
            parameters->size = va_arg(ap, int);
            break;
        case ReadFile:
        case WriteFile:
            parameters->fd   = va_arg(ap, int);        
            parameters->name = NULL;
            parameters->data = va_arg(ap, char*);
            parameters->size = va_arg(ap, int);
            break;
        default:
            free(parameters);
            return NULL;
    }

    

    va_end(ap); 
    
    return parameters;
}

int Requests_pushRequest(Queue queue, Request* request){
    int res;

    res = sem_wait(&(queue->make_request));
    if(res < 0){
        perror("Error while waiting semaphore to make a request");
        return -1;
    }

    res = sem_wait(&(queue->access_queue));
    if(res < 0){
        perror("Error while waiting semaphore to access queue requests");
        return -1;
    }

    printf("[Worker thread %d] Puntatore richiesta pushata %p\n\n", request->thread_creator, request);
    queue_enqueue(queue, (ElementAddr)(&request));

    res = sem_post(&(queue->access_queue));
    if(res < 0){
        perror("Error while posting semaphore to access queue requests");
        return -1;
    }

    res = sem_post(&(queue->take_request));
    if(res < 0){
        perror("Error while posting semaphore to take a request");
        return -1;
    }

    return 0;
}

int Requests_waitDispatchRequest(int id_thread){
    int res;

    res = sem_wait(&(requests_dispatched[id_thread].waiting_sem));
    if(res < 0){
        perror("Error while waiting semaphore to wait request dispatched");
        return -1;
    }

    return 0;
}

Request* Requests_popFirstRequest(Queue queue){
    int res;
    // Request** request_pop = (Request**)malloc(sizeof(Request*));
    Request* request = NULL;

    res = sem_wait(&(queue->take_request));
    if(res < 0){
        perror("[Main thread] Error while waiting semaphore to take a request");
        return NULL;
    }

    res = sem_wait(&(queue->access_queue));
    if(res < 0){
        perror("[Main thread] Error while waiting semaphore to access queue requests");
        return NULL;
    }

    queue_front(queue, (ElementAddr)(&request));
    // request = (*request_pop);
    // free(request_pop);
    printf("[Main thread] Puntatore richiesta poppata %p\n\n", (request));

    res = sem_post(&(queue->access_queue));
    if(res < 0){
        perror("[Main thread] Error while posting semaphore to access queue requests");
        return NULL;
    }

    res = sem_post(&(queue->make_request));
    if(res < 0){
        perror("[Main thread] Error while posting semaphore to make a request");
        return NULL;
    }
    
    return request;
}

int Requests_checkFileIsOpened(int id_thread, int fd){
    if(!(globalFHS[id_thread][fd])) return 0;
    else return 1;
}

int Requests_newFdFileHandle(int id_thread){
    for(int i = 0; i < MAX_N_FILE_OPEN; i++){
        if(!(globalFHS[id_thread][i])) return i;
    }
    return -1;
}

void Requests_checkDirectoryHandleThread(int id_thread){
    if(!globalDHS[id_thread]) globalDHS[id_thread] = SimpleFS_init(&fs, &disk);
}

int Requests_serveRequest(Queue queue, Request* request){
    int res = -1;

    Requests_checkDirectoryHandleThread(request->thread_creator);

    switch (request->parameters->request_type) {
        case MakeDir:
            res = Request_serveMakeDirRequest(request);
            break;
        case ChangeDir:
            res = Request_serveChangeDirRequest(request);
            break;
        case CreateFile:
            res = Request_serveCreateFileRequest(request);
            break;
        case CloseFile:
            res = Request_serveCloseFileRequest(request);
            break;
        case OpenFile:
            res = Requests_serveOpenFileRequest(request);
            break;
        case WriteFile:
            res = Requests_serveWriteFileRequest(request);
            break;
        case SeekFile:
            res = Requests_serveSeekFileRequest(request);
            break;
        case ReadFile:
            res = Requests_serveReadFileRequest(request);
            break;
        case Remove:
            res = Requests_serveRemoveRequest(request);
            break;
        default:
            printf("Unknown request... \n\n");
            break;
    }

    //setto ret value e sveglio thread
    requests_dispatched[request->thread_creator].ret_value = res;
    res = sem_post(&(requests_dispatched[request->thread_creator].waiting_sem));
    if(res < 0){
        perror("Error while posting on dispatched request semaphore");
        return -1;
    }

    queue_dequeue(queue);

    return 0;
}

int Request_serveMakeDirRequest(Request* request){
    return SimpleFS_mkDir(globalDHS[request->thread_creator], request->parameters->name);
}

int Request_serveChangeDirRequest(Request* request){
    return SimpleFS_changeDir(globalDHS[request->thread_creator], request->parameters->name);
}

int Request_serveCreateFileRequest(Request* request){
    int fd;
    fd = Requests_newFdFileHandle(request->thread_creator);

    if(fd < 0){
        printf("[Main thread] Too much files opened for thread %d\n\n", request->thread_creator);
    }
    else{
        globalFHS[request->thread_creator][fd] = SimpleFS_createFile(globalDHS[request->thread_creator], request->parameters->name);
    }

    return fd;
}

int Request_serveCloseFileRequest(Request* request){
    int res;

    res = SimpleFS_close(globalFHS[request->thread_creator][request->parameters->fd]);
    if(!res){
        globalFHS[request->thread_creator][request->parameters->fd] = NULL;
        return 0;
    }
    return -1;
}

int Requests_serveOpenFileRequest(Request* request){
    int fd;
    fd = Requests_newFdFileHandle(request->thread_creator);

    if(fd < 0){
        printf("[Main thread] Too much files opened for thread %d\n\n", request->thread_creator);
    }
    else{
        globalFHS[request->thread_creator][fd] = SimpleFS_openFile(globalDHS[request->thread_creator], request->parameters->name);
    }

    if(Requests_checkFileIsOpened(request->thread_creator, fd)) return fd;
    else return -1;
}

int Requests_serveWriteFileRequest(Request* request){
    if(!Requests_checkFileIsOpened(request->thread_creator, request->parameters->fd)){
        printf("File descriptor invalid\n\n");
        return -1;
    }

    return SimpleFS_write(globalFHS[request->thread_creator][request->parameters->fd], request->parameters->data, request->parameters->size);
}

int Requests_serveReadFileRequest(Request* request){
    if(!Requests_checkFileIsOpened(request->thread_creator, request->parameters->fd)){
        printf("File descriptor invalid\n\n");
        return -1;
    }

    return SimpleFS_read(globalFHS[request->thread_creator][request->parameters->fd], request->parameters->data, request->parameters->size);
}

int Requests_serveSeekFileRequest(Request* request){
    if(!Requests_checkFileIsOpened(request->thread_creator, request->parameters->fd)){
        printf("File descriptor invalid\n\n");
        return -1;
    }

    return SimpleFS_seek(globalFHS[request->thread_creator][request->parameters->fd], request->parameters->size);
}

int Requests_serveRemoveRequest(Request* request){
    return SimpleFS_remove(globalDHS[request->thread_creator], request->parameters->name);
}

void Requests_freeRequestFunction(ElementAddr elemaddr){

    Request* request = (*(Request**)elemaddr);
    printf("[Main thread] Freeing request at %p\n\n", request);

    free(request->parameters);
    free(request);
}

ThreadArgs* Requests_threadArgsInit(Queue queue, int id_thread){
    ThreadArgs* threadArgs = (ThreadArgs*)malloc(sizeof(ThreadArgs));

    threadArgs->queue = queue;
    threadArgs->id_thread = id_thread;

    return threadArgs;
}

void *thread_workload_1(void* args){
    int fd;
    ThreadArgs* threadArgs = (ThreadArgs*)args;
    Request* my_request;

    printf("[Worker thread %d] Thread ready to make requests\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(MakeDir, "Dir1_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while creating dir by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has created a dir!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(ChangeDir, "Dir1_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while changing dir by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has changed dir!\n\n", threadArgs->id_thread);
    
    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(MakeDir, "Dir1_1"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while creating dir by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has created a dir!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(CreateFile, "File1_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    fd = requests_dispatched[threadArgs->id_thread].ret_value;
    if(fd < 0) printf("Error while creating file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has created a file with fd %d!\n\n", threadArgs->id_thread, fd);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(CloseFile, fd));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while closing file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has close a file!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(OpenFile, "File1_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    fd = requests_dispatched[threadArgs->id_thread].ret_value;
    if(fd < 0) printf("Error while opening file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has opened a file with fd %d!\n\n", threadArgs->id_thread, fd);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(WriteFile, fd, "CIAO AMORE MIO SONO IL THREAD CHE HA SCRITTO SU FILEEEEEE YEEEE", 64));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while writing file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has wrote a file!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(SeekFile, fd, 0));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while seeking file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has seek a file!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(WriteFile, fd, "UEFIHSDIUAHFSDUIFHSUIH", 22));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while writing file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has wrote a file!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(CloseFile, fd));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while closing file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has close a file!\n\n", threadArgs->id_thread);

    free(globalDHS[threadArgs->id_thread]->dcb);
    free(globalDHS[threadArgs->id_thread]->p_directory);
    free(globalDHS[threadArgs->id_thread]);
    free(threadArgs);

    return 0;
}

void *thread_workload_0(void* args){
    int fd;
    ThreadArgs* threadArgs = (ThreadArgs*)args;
    Request* my_request;

    printf("[Worker thread %d] Thread ready to make requests\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(MakeDir, "Dir0_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while creating dir by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has created a dir!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(ChangeDir, "Dir0_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while changing dir by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has changed dir!\n\n", threadArgs->id_thread);
    
    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(MakeDir, "Dir0_1"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while creating dir by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has created a dir!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(CreateFile, "File0_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    fd = requests_dispatched[threadArgs->id_thread].ret_value;
    if(fd < 0) printf("Error while creating file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has created a file with fd %d!\n\n", threadArgs->id_thread, fd);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(CloseFile, fd));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while closing file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has close a file!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(OpenFile, "File0_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    fd = requests_dispatched[threadArgs->id_thread].ret_value;
    if(fd < 0) printf("Error while opening file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has opened a file with fd %d!\n\n", threadArgs->id_thread, fd);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(WriteFile, fd, "SONO MOLTO FRUFRU E SCRIVO NDO CAZZO ME PARE", 45));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while writing file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has wrote a file!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(SeekFile, fd, 5));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while seeking file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has seek a file!\n\n", threadArgs->id_thread);

    char data_read[13] = {0};

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(ReadFile, fd, data_read, 12));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while reading file by thread %d\n\n", threadArgs->id_thread);
    else{
        printf("[Worker thread %d] Thread has read a file!\n\n", threadArgs->id_thread);
        printf("[Worker thread %d] Data read: %s\n\n", threadArgs->id_thread, data_read);
    }

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(CloseFile, fd));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while closing file by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has close a file!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(ChangeDir, ".."));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while changing dir by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has changed dir!\n\n", threadArgs->id_thread);

    my_request = Requests_requestInit(threadArgs->id_thread, Requests_parametersInit(Remove, "Dir0_0"));
    printf("[Worker thread %d] Thread has created a request\n\n", threadArgs->id_thread);
    Requests_pushRequest(threadArgs->queue, my_request);
    printf("[Worker thread %d] Thread has pushed a request\n\n", threadArgs->id_thread);
    printf("[Worker thread %d] Thread is waiting for dispatching of his request\n\n", threadArgs->id_thread);
    Requests_waitDispatchRequest(threadArgs->id_thread);
    if(requests_dispatched[threadArgs->id_thread].ret_value < 0) printf("Error while removing dir and its content by thread %d\n\n", threadArgs->id_thread);
    else printf("[Worker thread %d] Thread has removed a dir and its content!\n\n", threadArgs->id_thread);

    free(globalDHS[threadArgs->id_thread]->dcb);
    free(globalDHS[threadArgs->id_thread]->p_directory);
    free(globalDHS[threadArgs->id_thread]);
    free(threadArgs);

    return 0;
}

void *main_thread_routine(void* args){
    ThreadArgs* threadArgs = (ThreadArgs*)args;
    Request* request_to_serve;

    printf("[Main thread] Main thread started. Ready to serve requests\n\n");
    while(1){
        printf("[Main thread] Waiting for a request...\n\n");
        request_to_serve = Requests_popFirstRequest(threadArgs->queue);
        Requests_serveRequest(threadArgs->queue, request_to_serve);
        printf("[Main thread] Request served successfully\n\n");
    }

    return 0;
}

void BeEvilWhenTestingRequests(){
    int res, i;

    printf("\n[Main process] Initializing disk...\n\n");
    DiskDriver_init(&disk, "FAPDisk", 10000);

    printf("[Main process] Initializing queue...\n\n");
    Queue queue = Requests_queueRequestsInit(Requests_freeRequestFunction);

    printf("[Main process] Initializing global semaphores\n\n");
    Requests_globalSemaphoresInit();

    printf("[Main process] Initializing global directory handles\n\n");
    Requests_directoryHandlesInit();

    printf("[Main process] Initializing global file handles\n\n");
    Requests_fileHandlesInit();

    pthread_t threads[N_THREADS + 1];

    for(i = 0; i < (N_THREADS + 1); i++){
        ThreadArgs* ta = Requests_threadArgsInit(queue, i);
        if(i == N_THREADS){
            res = pthread_create(&(threads[i]), NULL, main_thread_routine, ta);
            if(res < 0){
                perror("[Main process] Error while creating thread");
                exit(-1);
            }
        }
        else if(i == 0){
            res = pthread_create(&(threads[i]), NULL, thread_workload_0, ta);
            if(res < 0){
                perror("[Main process] Error while creating thread");
                exit(-1);
            }
        }
        else{
            res = pthread_create(&(threads[i]), NULL, thread_workload_1, ta);
            if(res < 0){
                perror("[Main process] Error while creating thread");
                exit(-1);
            }
        }
        printf("[Main process] Thread %d create successfully\n\n", i);
    }

    for(i = 0; i < N_THREADS + 1; i++) pthread_join(threads[i], NULL);
}