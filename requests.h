#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdarg.h>
#include "generic_queue.h"
#include "simplefs.h"

#define N_THREADS 2
#define MAX_N_FILE_OPEN 10
#define MAX_N_REQUESTS 20

typedef enum{CreateFile, OpenFile, CloseFile, ReadFile, WriteFile, SeekFile, ChangeDir, MakeDir, Remove}RequestType;

typedef struct{
    RequestType request_type;
    int fd;
    char* name;
    char* data;
    int size;
} Parameters;

typedef struct{
    int thread_creator;
    Parameters* parameters;
} Request;

typedef struct{
    int id_thread;
    Queue queue;
} ThreadArgs;

typedef struct{
    sem_t waiting_sem;
    int ret_value;
} RequestDispatched;

//Set all directory handles of threads to serve on root
void Requests_directoryHandlesInit();

//Initializes all the global semaphore for dispatching
void Requests_globalSemaphoresInit();

//Set all a null cause at start there is no file open
void Requests_fileHandlesInit();

//Add a new file handle of a thread
void Requests_addFileHandle(FileHandle* fileHandle, int id_thread, int fd);

//Initialize the desired queue
Queue Requests_queueRequestsInit(PfCbFree freefn);

//Initialize a Request struct
Request* Requests_requestInit(int thread_creator, Parameters* parameters);

//Initialize a InitialParameters struct
Parameters* Requests_parametersInit(RequestType request_type, ...);

//Push to the end of queue a new Request
int Requests_pushRequest(Queue queue, Request* request);

//Wait for dispatching of request
int Requests_waitDispatchRequest(int id_thread);

//Take first request of the queue to serve if there is one
Request* Requests_popFirstRequest(Queue queue);

//Return 0 if the file is not open, 1 if it is
int Requests_checkFileIsOpened(int id_thread, int fd);

//return file descriptor of new file if there is space
int Requests_newFdFileHandle(int id_thread);

//if it is the first request create a dir handle for thread
void Requests_checkDirectoryHandleThread(int id_thread);

//serve a request
int Requests_serveRequest(Queue queue, Request* request);

//wrapper 
int Request_serveMakeDirRequest(Request* request);

//wrapper
int Request_serveChangeDirRequest(Request* request);

//wrapper
int Request_serveCreateFileRequest(Request* request);

//wrapper
int Request_serveCloseFileRequest(Request* request);

//wrapper
int Requests_serveOpenFileRequest(Request* request);

//wrapper
int Requests_serveSeekFileRequest(Request* request);

//wrapper
int Requests_serveRemoveRequest(Request* request);

//wrapper
int Requests_serveReadFileRequest(Request* request);

//wrapper
int Requests_serveWriteFileRequest(Request* request);

//free elem of queue
void Requests_freeRequestFunction(ElementAddr elemaddr);

//debug function
void BeEvilWhenTestingRequests();

