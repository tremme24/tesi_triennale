#pragma once
#include <stdlib.h>
#include <stdio.h>
#include "disk_driver.h"

#define MAX_ENTRIES_IN_FDB (BLOCK_SIZE - sizeof(BlockHeader) - sizeof(FileControlBlock) - sizeof(int)) / sizeof(int)
#define MAX_ENTRIES_IN_DB  (BLOCK_SIZE - sizeof(BlockHeader)) / sizeof(int)
#define MAX_BYTE_IN_FFB (BLOCK_SIZE - sizeof(BlockHeader) - sizeof(FileControlBlock))
#define MAX_BYTE_IN_FB  (BLOCK_SIZE - sizeof(BlockHeader))

/*these are structures stored on disk*/

// header, occupies the first portion of each block in the disk
// represents a chained list of blocks
typedef struct {
  int previous_block; // chained list (previous block)
  int next_block;     // chained list (next_block)
  int block_in_file; // position in the file, if 0 we have a file control block
} BlockHeader;


// this is in the first block of a chain, after the header
typedef struct {
  int directory_block; // first block of the parent directory
  int block_in_disk;   // repeated position of the block on the disk
  char name[128];
  int size_in_bytes;
  int size_in_blocks;
  int index_last_block;
  int is_dir;          // 0 for file, 1 for dir
} FileControlBlock;

// this is the first physical block of a file
// it has a header
// an FCB storing file infos
// and can contain some data

/******************* stuff on disk BEGIN *******************/
typedef struct {
  BlockHeader header;
  FileControlBlock fcb;
  char data[BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader)] ; //352byte
} FirstFileBlock;

// this is one of the next physical blocks of a file
typedef struct {
  BlockHeader header;
  char  data[BLOCK_SIZE-sizeof(BlockHeader)]; //500byte
} FileBlock;

// this is the first physical block of a directory
typedef struct {
  BlockHeader header;
  FileControlBlock fcb;
  int num_entries;
  int file_blocks[ (BLOCK_SIZE
		   -sizeof(BlockHeader)
		   -sizeof(FileControlBlock)
		    -sizeof(int))/sizeof(int) ];
} FirstDirectoryBlock;

// this is remainder block of a directory
typedef struct {
  BlockHeader header;
  int file_blocks[ (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int) ];
} DirectoryBlock;
/******************* stuff on disk END *******************/




  
typedef struct {
  DiskDriver* disk;
  int root; //index of FDB of root 
  int curr_dir; //index of FDB of current Dir
} SimpleFS;

// this is a file handle, used to refer to open files
typedef struct {
  SimpleFS* sfs;                   // pointer to memory file system structure
  FirstFileBlock* fcb;             // pointer to the first block of the file(read it)
  FirstDirectoryBlock* p_directory;// pointer to the directory where the file is stored
  BlockHeader* current_block;      // current block in the file
  int pos_in_file;                 // position of the cursor
} FileHandle;

typedef struct {
  SimpleFS* sfs;                   // pointer to memory file system structure
  FirstDirectoryBlock* dcb;        // pointer to the first block of the directory(read it)
  FirstDirectoryBlock* p_directory;// pointer to the parent directory (null if top level)
  BlockHeader* current_block;      // current block in the directory
  int pos_in_dir;                  // absolute position of the cursor in the directory
  int pos_in_block;                // relative position of the cursor in the block
} DirectoryHandle;


//info file/dir to remove
typedef struct{
  int index_relative_of_block_directory;
  int index_on_disk;
  int entry;
} IndexesEntryOfFile;

//to store file opened/created
typedef struct{
  char* filename;
  FileHandle* fh;
} Entry_table_file_opened;


// initializes a file system on an already made disk
// returns a handle to the top level directory stored in the first block
DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk);

//Init a DirectoryHandle struct with the index of first directory block on disk
void SimpleFS_directoryHandleInit(SimpleFS* sfs, DirectoryHandle* directoryHandle,  int fdb_index);

//Alloc a DirectoryHandle struct
DirectoryHandle* SimpleFS_directoryHandleAlloc();

//Init a new DirectoryBlock struct after extension
DirectoryBlock SimpleFS_directoryBlockInit(int previous_block, int block_in_file);

//Init a FirstDirectoryBlock with directory_block index for fdb of parent,
//positioned at index block_in_disk on disk and with name = name
FirstDirectoryBlock SimpleFS_firstDirectoryBlockInit(int p_directory_block, int block_in_disk, const char* name);

// creates the inital structures, the top level directory
// has name "/" and its control block is in the first position
// it also clears the bitmap of occupied blocks on the disk
// the current_directory_block is cached in the SimpleFS struct
// and set to the top level directory
void SimpleFS_format(SimpleFS* fs);

//return the block index in disk if there is in the current directory a file/dir whit name = name
int SimpleFS_findFcbByNameInCurrentDirectory(DirectoryHandle* d, const char* name, int is_dir);

//create a FileBlock struct to extend file
FileBlock SimpleFS_fileBlockInit(int previous_block, int block_in_file);

//create a FirstFileBlock struct for an empty file
FirstFileBlock SimpleFS_firstFileBlockInit(int p_directory_block, int block_in_disk, const char* name);

// creates an empty file in the directory d
// returns null on error (file existing, no free blocks)
// an empty file consists only of a block of type FirstBlock
FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename);

// reads in the (preallocated) blocks array, the name of all files in a directory 
int SimpleFS_readDir(char** names, DirectoryHandle* d);

//alloc a FileHandle struct
FileHandle* SimpleFS_fileHandleAlloc();

//init a FileHandle struct
void SimpleFS_fileHandleInit(SimpleFS* sfs, DirectoryHandle* directoryHandle, FileHandle* fileHandle, int ffb_index);

// opens a file in the  directory d. The file should be exisiting
FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename);

// closes a file handle (destroyes it)
int SimpleFS_close(FileHandle* f);

//find index on disk from a relative index in file
int SimpleFS_findBlockFileByBlockInFile(FileHandle* f, int block_in_file);

//add n_file_blocks_to_add file blocks to file to store data
int SimpleFS_extendCurrentFile(FileHandle* f, int n_file_blocks_to_add);

//remove last block from current directory
int SimpleFS_reduceCurrentDirectory(DirectoryHandle* d, FirstDirectoryBlock* fdb_last_block_directory, int idx_last_block_directory);
// writes in the file, at current position for size bytes stored in data
// overwriting and allocating new space if necessary
// returns the number of bytes written
int SimpleFS_write(FileHandle* f, void* data, int size);

// read in the file, at current position size bytes stored in data
// returns the number of bytes read
int SimpleFS_read(FileHandle* f, void* data, int size);

// returns the number of bytes read (moving the current pointer to pos)
// returns pos on success
// -1 on error (file too short)
int SimpleFS_seek(FileHandle* f, int pos);

// seeks for a directory in d. If dirname is equal to ".." it goes one level up
// 0 on success, negative value on error
// it does side effect on the provided handle
int SimpleFS_changeDir(DirectoryHandle* d, char* dirname);

//add another block if there is enough space on disk to the current directory
int SimpleFS_extendCurrentDirectory(DirectoryHandle* d, FirstDirectoryBlock* fdb_last_block_directory, int idx_last_block_directory);

// creates a new directory in the current one (stored in fs->current_directory_block)
// 0 on success
// -1 on error
int SimpleFS_mkDir(DirectoryHandle* d, char* dirname);

//find indexes and entry of file/dir to remove
IndexesEntryOfFile SimpleFS_findForRemove(DirectoryHandle* d, const char* name);

//Reset Bitmap for file 
int SimpleFS_freeAllBlocksOfFile(DirectoryHandle* d, FirstFileBlock* ffb_to_remove);

// removes the file in the current directory
// returns -1 on failure 0 on success
// if a directory, it removes recursively all contained files
// pay attention to not format the blocks but only de-reference it in bmap
// and in diskheader struct accordingly
int SimpleFS_remove(DirectoryHandle* d, char* filename);

//save changes of DirectoryHandle struct on disk
void SimpleFS_saveDirectoryHandleOnDisk(DirectoryHandle* d);

//After an operation on a file we need to sync memory 
//with the changes made by the fileß
int SimpleFS_syncCurrDir(DirectoryHandle* d);

//if 1 add else remove
int SimpleFS_updateSizeDirectory(DirectoryHandle* d, int size_in_bytes_to_remove, int add_or_remove);

//print tree view of file system
void SimpleFS_tree(SimpleFS* fs);

//print main information about current directory
void printDir(DirectoryHandle* d);

//print main information about file
void printFile(FileHandle* f);

//debug function
void BeEvilWhenTestingSimpleFS(SimpleFS* fs, DiskDriver* disk, const char* name_of_disk, int num_of_blocks);


