# **Documentation**

## What is this?

*This is an analysis in-depth of the programming involved in this File System, which means function-wise explaination and thoughts.*

#  

## Introduction
Our **FS** is based on a file of a given size that's need to be initialized as a "disk" and organized in "Blocks".This meaning that it needed a header, containing some disk infos, a bitmap, and a way for our Directory and File storage to be coherent with the disk.

***NOTE 1**: almost all the struct were given by our professors, same for the function declaration, we modified something as we will explain when necessary later in this documentation* 

***NOTE 2**: the code showed here is stripped to make this more readable, and also we purposelly omitted purely debug functions, if you want the full code, the project is public :)*

#

## Bitmap

The project started by creating the bitmap, a data structure utilizied in our FS to map every block to a "bit" in our struct useful for knowing if the given block is utilized ( 1 ) or free ( 0 ).
The bitmap need to be stored on the disk as a header.

### *Structs*
A basic struct contaning an array and an int with the number of bit needed for storing information for all the block (basically it's the same as the number in the block in disk):

    typedef struct{
        int num_bits; 
        char* entries; // Every char can store informations of 8 blocks
    } BitMap;
   
This is more a "support" struct, useful for converting the block number to an actual bit in the array

    typedef struct{
        int entry_num; 
        char bit_num; // Offset in the char block in entries
    } BitMapEntryKey;




### *Functions*

* BitMapEntryKey BitMap_blockToIndex(int num);
  
      A functions that, given a number of a block in the disk returns a structure     BitMapEntryKey containing the calculated data for accessing the bit in the bitmap that contains the usage infomarmation of the block 'num'.
      
        BitMapEntryKey bmek;
        bmek.entry_num = num / 8; // Calculate char in the array which contain the bit
        bmek.bit_num = num % 8; // Calculate the offset inside the 'entry_num' char
      

* int BitMap_indexToBlock(int entry, uint8_t bit_num);
  
      Opposite the the last function, given an entry and an offset it returns the block number, simply:

          entry*8 + bit_num;

* int BitMap_get(BitMap* bmap, int start, int status);

      This function return the index of the first bit with value equal to status (0 or 1), searched from the index 'start' or returns -1 if any error occured.
      
      This is done with a double cycle, first finding the block in the bmap:
          unsigned int idx = start/8;
          while(idx <= (bmap->num_bits)/8)
              unsigned char block = bmap->entries[idx];
            
      then searching the bit:
          for(int i = 7; i >= 0; i--){
              char bit = (block >> i) % 2;
                  if(bit == status) return (7-i)+idx*8; 

* int BitMap_set(BitMap* bmap, int pos, int status);

      The set function uses a mask and bit-wise operator for setting the bit in the bitmap representing the block number 'pos' to status. 
      Returns -1 on error and 0 on success.
        
      If bit need to be set at 1,create a mask with a 1 on the desired bit and use an OR:
          if(status)   
              mask = status << (7 - bit);
              bmap->entries[idx] = BLOCK | mask; 

      If bit need to be set at 0, create a mask with all 1 except a 0 on the desired bit and use an AND:
          if(!status)
              mask = 0xFF - (1 << (7 - bit)); 
              bmap->entries[idx] = BLOCK & mask;
        
* unsigned char BitMap_isBusy(BitMapEntryKey* entry, char* bitmap)
      
      This is a support function created with the purpose of having a way of knowing if a block is used, it simply returns 0 if unused and 1 if used:
          (BLOCK_IN_BITMAP >> (7 - OFFSET_IN_CHAR)) % 2;
        

Managing this wasn't the hardest part of the project for sure, and it went pretty smoothly with only one day of coding and half a day of debugging needed.

#

## Disk Driver

Following our 'LOW to HIGH level' approach on this project we started immediately working on the DiskDriver, a couple of structs and some functions dedicated to manage the file acting as a Disk(headers etc.) and the 'block' aritmethics.

### *Structs*

This first struct is stored in the first block of the Disk, after the Bitmap, contaning various useful infos:

    typedef struct{
        int num_blocks; // Number of total blocks in the disk
        int bitmap_blocks; // Blocks occupied by the bitmap in the disk
        int bitmap_entries; 
        int free_blocks; // Number of free block
        int first_free_block; // Index of the first free block
    } DiskHeader; 

This struct is useful for accessing the data of the disk inside a process, see the init for how we implemented it.

    typedef struct{
        DiskHeader* header; 
        char* bitmap_data; 
        int fd; // Will contain the FileDescriptor of our 'FS'
    } DiskDriver;

### *Functions*

* void DiskDriver_init(DiskDriver* disk, const char* filename, int num_blocks);
        
      This function is aimed to initialize a Disk named 'filename' (as said before, we treat a binary file as a disk) sized num_blocks*BLOCK_SIZE.
      The function starts with checking if the disk not exist and in case it's initialized with an open:

          int fd = open(filename, O_CREAT | O_RDWR | O_EXCL , 0777);

      then we strech it to the choosen size: 

          int res = lseek(fd, filesize-1, SEEK_SET);
          res = write(fd, "" , 1);

      and initialize in stack an instance of a struct DiskHeader and BitMap, pre-set correctly considering the blocks necessary for these structures, to write in the first(or more) block(s) of the disk:

          DiskHeader diskHeader_temp;
          diskHeader_temp.num_blocks = num_blocks; 
          diskHeader_temp.bitmap_blocks = bitmap_blocks;
          diskHeader_temp.bitmap_entries = bitmap_entries;
          diskHeader_temp.free_blocks = num_blocks - bitmap_blocks;
          diskHeader_temp.first_free_block = bitmap_blocks;
    
          char map[bitmap_entries];
          BitMap temp = {0};
          temp.entries=map;
          temp.num_bits=num_blocks;
          for(int i=0;i<bitmap_blocks;i++) BitMap_set(&temp,i,1);

          res = write(fd, map, bitmap_entries);

      Then the headers need to be loaded in RAM, having a way to know the states of the blocks without accessing to the disk. 
          
          disk->fd = fd;
          char* result = mmap(NULL, (bitmap_blocks)*BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
          disk->bitmap_data = result + sizeof(DiskHeader);
          disk->header = (DiskHeader*)result;

      NOTE: Bitmap and header needs to be written back to the disk when closing 
            the driver, meaning also that this FS needs semaphores in order to be 
            used from more than one process
       
* int DiskDriver_readBlock(DiskDriver* disk, void* dest, int block_num);
      
      Reads from the disk block 'block_num' and stores it into a buffer 'dest'. 
      It's simply a couple of checks for errors and a read:

          pread(disk->fd, dest, BLOCK_SIZE, block_num*BLOCK_SIZE)

* int DiskDriver_writeBlock(DiskDriver* disk, void* src, int block_num);
      
      Writes into the disk block 'block_num' reading from a buffer 'dest' and set the bitmap and header(free_blocks and first_free_blocks) accordingly.
      Same as DiskDriver_readBlock but with a write plus it needs a BitMap structure (we only saved the entries buffer in memory):

          BitMap temp = {0};
          temp.entries = disk->bitmap_data;
          temp.num_bits = disk->header->num_blocks;
          BitMap_set(&temp, block_num, 1);
          disk->header->free_blocks -= 1;
          pwrite(disk->fd, src, BLOCK_SIZE, block_num*BLOCK_SIZE) < 0

* int DiskDriver_freeBlock(DiskDriver* disk, int block_num);
      
      Frees the disk block 'block_num'(if it's in use) wiping it and setting the bitmap and header accordingly:
        src[BLOCK_SIZE] = {0};
        pwrite(disk->fd, src, BLOCK_SIZE, block_num*BLOCK_SIZE);

* int DiskDriver_updateBlock(DiskDriver* disk, void* src, int block_num);
  
      Support function created by us to help in managing the situation where you can't overwrite a block because of it's bit in the bitmap set to 1. Mainly we used it for updating BitMap and Headers. 
      
      Calls a DiskBlock_free and a DiskBlock_write.

* int DiskDriver_getFreeBlock(DiskDriver* disk, int start);
  
      Return the index of the first free block available from the position 'start' checking the bitmap.
      We actually call the BitMap_get function (see above) but initializing a temporary BitMap struct:

          BitMap temp = {0};
          temp.entries = disk->bitmap_data;
          temp.num_bits = disk->header->num_blocks;
          int res = BitMap_get(&temp,start,0); 

* int DiskDriver_flush(DiskDriver* disk);
      
      Synch the memory mapped Header and Bitmap to the disk, by calling the syscall:

          int res = msync((char*)disk->header, (disk->header->bitmap_blocks)*BLOCK_SIZE , MS_SYNC);

This part was a bit more challanging to complete, mainly because we had to make some decision on how we wanted to manage the headers of the disk.

## File System

This is the most challanging part of our project due to the ammount of struct and case possibile in a disk when writing/reading a file from

### *Struct*
    
These are the structs that are going to be saved on disk (throught some other struct containing) them to manage files and directories accordingly and are common to both:

    typedef struct 
        int previous_block; // chained list (previous block)
        int next_block;     // chained list (next_block)
        int block_in_file; // position in the file, if 0 we have a file control block
    } BlockHeader;

    typedef struct{
        int directory_block; // Position of the FDB 
        int block_in_disk;   // Position of the FFB in disk
        char name[128];
        int size_in_bytes; // Bytes of data 
        int size_in_blocks; // Block occupied by the file
        int is_dir;          // Set to 0 if is a file, 1 if is a directory
    } FileControlBlock;


These are the struct that needs to actually be saved to the disk, and has a BlockHeader an FCB and an array of char for storing data, different in size between the first block and the others:

    typedef struct{
        BlockHeader header;
        FileControlBlock fcb;
        char data[BLOCK_SIZE-sizeof(FileControlBlock) - sizeof(BlockHeader)] ; // Data for the firstFileBlock
    } FirstFileBlock;

    typedef struct{
        BlockHeader header;
        char  data[BLOCK_SIZE-sizeof(BlockHeader)]; // Data for non-firstFileBlock
    } FileBlock;

    typedef struct{
        BlockHeader header;
        FileControlBlock fcb;
        int num_entries;
        int file_blocks[ (BLOCK_SIZE
                -sizeof(BlockHeader)
                -sizeof(FileControlBlock)
                    -sizeof(int))/sizeof(int) ]; // Data for the firstDirectoryBlock
    } FirstDirectoryBlock;

    typedef struct{
        BlockHeader header;
        int file_blocks[ (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int) ]; // Data for non-firstDirectoryBlock
    } DirectoryBlock;

These struct are a property of the process running the File Systems and needs to be stored in stack. 

We added to the SimpleFS struct two indexes, one for the block containing the FDB for 'root' or '/', that will never change, and one for the current directory, and we left unchanged the others structs:

    typedef struct{
        DiskDriver* disk;
        int root; //index of FDB of root 
        int curr_dir; //index of FDB of current Dir
    } SimpleFS;

    typedef struct{
        SimpleFS* sfs;                   // Pointer to the FS struct
        FirstFileBlock* fcb;             // Pointer to the FirstFileBlock, that needs to be read from disk
        FirstDirectoryBlock* p_directory;  // Pointer to the parent directory of the file
        BlockHeader* current_block;      // Current block header in the file
        int pos_in_file;                 // Position of the cursor in the file
    } FileHandle;

    typedef struct{
        SimpleFS* sfs;                   
        FirstDirectoryBlock* dcb;        
        FirstDirectoryBlock* p_directory;  
        BlockHeader* current_block;      
        int pos_in_dir;                  // Absolute position of the cursor in the directory
        int pos_in_block;                // Absolute position of the cursor in the block
    } DirectoryHandle;

   This is a support struct created by us for handling many FileHandle opened:

    typedef struct{
        char* filename;
        FileHandle* fh;
    } Entry_table_file_opened;



### *Functions*

* DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk);
      
      This function initialize an already made Disk with an already made FS, and returns a DirectoryHandle that we allocate. Next we fill the fields of the fs, disk and the DirHandle to return.

              DirectoryHandle* d_h = (DirectoryHandle*)malloc(sizeof(DirectoryHandle));
              d_h->sfs = fs;
              d_h->dcb = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock)); 
              d_h->p_directory = NULL; 

              memcpy(d_h->dcb,tmp,sizeof(FirstDirectoryBlock));
              d_h->current_block = &(d_h->dcb->header);
              d_h->pos_in_dir = 0;
              d_h->pos_in_block = disk->header->bitmap_blocks;

* void SimpleFS_format(SimpleFS* fs);
  
      Format the disk creating the initial structures (FDB) and the root directory, assigning to it the name '/', also clears the bitmap
          
          FirstDirectoryBlock tmp_fdb = {0};

          //We then assign the correct data and write the FDB into the disk;

          char tmp[BLOCK_SIZE] = {0};
          memcpy(tmp,&tmp_fdb,sizeof(FirstDirectoryBlock));
          res = DiskDriver_writeBlock(fs->disk,tmp,tmp_fdb.fcb.block_in_disk);
          

* FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename);
      
      Creates an empty file in the directory d, in fact it write only the First File Block of the file, a tricky problem was the necessity of a new Directory Block in case that the First Directory Block 'data' array were full.
      It returns a filehandle of the file created.

          //We check if the file already exist in the dir and in case return an error then continue 
          
          FirstFileBlock ffb_tmp = {0};
          ffb_tmp.header.previous_block = -1;
          ffb_tmp.header.next_block = -1;
          ffb_tmp.header.block_in_file = 0;
          ffb_tmp.fcb.directory_block = d->dcb->fcb.block_in_disk;
          ffb_tmp.fcb.block_in_disk = idx_block_tmp;
          ffb_tmp.fcb.size_in_bytes = 0;
          ffb_tmp.fcb.size_in_blocks = 1;
          ffb_tmp.fcb.is_dir = 0;
            
          memcpy(&(ffb_tmp.fcb.name), filename ,strlen(filename));
          char ffb_block[BLOCK_SIZE] = {0};
          memcpy(ffb_block, &ffb_tmp, sizeof(FirstFileBlock));
        
          res = DiskDriver_writeBlock(d->sfs->disk, ffb_block, ffb_tmp.fcb.block_in_disk);
    
          FileHandle* to_ret = (FileHandle*)malloc(sizeof(FileHandle));
          to_ret->fcb = (FirstFileBlock*)malloc(sizeof(FirstFileBlock));
          to_ret->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));

          to_ret->sfs = d->sfs;
          memcpy(to_ret->fcb, &ffb_tmp, sizeof(FirstFileBlock));
          memcpy(to_ret->p_directory, d->dcb, sizeof(FirstDirectoryBlock));
          to_ret->current_block = &(to_ret->fcb->header);
          to_ret->pos_in_file = 0;

* int SimpleFS_readDir(char** names, DirectoryHandle* d);

      This function is aimed to read all the files and directory in a given directory 'd' and to write all the names into the buffer char** names:

        //We read all the files in the First Directory Block

          char read_block_file_tmp[BLOCK_SIZE] = {0};
          for(; cnt < idx_max_fb; cnt++){
              idx_block_file = d->dcb->file_blocks[cnt];
              if(idx_block_file == 0) break;
              res = DiskDriver_readBlock(d->sfs->disk, read_block_file_tmp, idx_block_file);
              FirstFileBlock* ffb_tmp = (FirstFileBlock*)read_block_file_tmp;
              memcpy(names[cnt], ffb_tmp->fcb.name, 128);
          }

        //and if needed we search for files in other blocks of type DirectoryBlock, changing the idx_max_fb and the directory handle accordingly

          while(cnt < d->dcb->num_entries){
          res = DiskDriver_readBlock(d->sfs->disk, read_block_dir_tmp, d->current_block->next_block);
          DirectoryBlock* db_tmp = (DirectoryBlock*)read_block_dir_tmp;
          d->current_block = &(db_tmp->header);
          idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int);
          for(int i=0; i < idx_max_fb; i++){
              //SAME AS ABOVE CYCLE
              cnt++;
              }
          }
      
* FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename);

      Open a the file named filename inside the directory d. Same as the createFile, we need to find the file in the array that can be in the first directory block or the next directory block.
          
          //We read all the names of the files in the folder, then when we find it we set a flag to 1 to check later.
          //the loop useful to find the files is pretty much identical to the one of readDir

          FileHandle* to_ret = NULL;
          if(found){
          to_ret = (FileHandle*)malloc(sizeof(FileHandle));
          to_ret->fcb = (FirstFileBlock*)malloc(sizeof(FirstFileBlock));
          to_ret->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));

          to_ret->sfs = d->sfs;
          memcpy(to_ret->fcb, ffb_tmp, sizeof(FirstFileBlock));
          memcpy(to_ret->p_directory, d->dcb, sizeof(FirstDirectoryBlock));
          to_ret->current_block = &(to_ret->fcb->header);
          to_ret->pos_in_file = 0;
          }
          
* DirectoryHandle* SimpleFS_createDirHandle(FirstDirectoryBlock* fdb, SimpleFS* fs);

      
      This is a support function created by us used when we need to sync the changes to our FDB to the disk

      DirectoryHandle* to_ret = (DirectoryHandle*)malloc(sizeof(DirectoryHandle));
        to_ret->dcb = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));

        to_ret->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
        int buf_tmp[BLOCK_SIZE] = {0};
        int res = DiskDriver_readBlock(fs->disk, buf_tmp, fdb->fcb.directory_block);
        if(res < 0) return NULL;
        memcpy(to_ret->p_directory, buf_tmp, sizeof(FirstDirectoryBlock));

        //We then modify the struct to_ret accordingly and returns it
  
* int SimpleFS_close(FileHandle* f);
  
      This function simply calls some close() on the allocated area of memory necessary to a FileHandle 
        
          if(!f) return 0;
          free(f->p_directory);
          free(f->fcb);
          free(f);


* int SimpleFS_write(FileHandle* f, void* data, int size);

      Writes in the file at the current position, stored in f->pos_in_file, overwriting and allocating space if necessary.
      Returns the number of bytes written or -1 on error.
          
          //We first find the block where we'll start writing, if in the First File Block or in the following File Block
          //Then we start writing and checking if the buffer enters in the block available, in case we need to create another block and set the header correctly (prev, next)
          char buf_tmp[BLOCK_SIZE] = {0};
          while(cnt < size && next > 0){
              toWrite = (size - cnt)<(max_fb-offset)?(size - cnt):(max_fb-offset);
              memcpy((fb_tmp->data+offset),aux,toWrite);
              res = DiskDriver_updateBlock(f->sfs->disk,read_block_file_tmp,next); 
              cnt += toWrite;
              aux += toWrite;
              int prev = next;
              next = fb_tmp->header.next_block;
              if(next > 0 && cnt < size){        
                  res = DiskDriver_readBlock(f->sfs->disk,buf_tmp,next);
              }
              if(toWrite == (max_fb-offset) && next < 0 && f->sfs->disk->header->free_blocks > 0){
                  next = DiskDriver_getFreeBlock(f->sfs->disk,f->sfs->disk->header->first_free_block);
                  FileBlock tmp = {0};
                  //filling fields with congruent data and writing back
                  res = DiskDriver_writeBlock(f->sfs->disk,buf_tmp,fb_tmp->header.next_block);
                  res = DiskDriver_updateBlock(f->sfs->disk,read_block_file_tmp,prev);
              }    
          memcpy(read_block_file_tmp,buf_tmp,BLOCK_SIZE);
          offset = 0;
          }
          if(f->pos_in_file + cnt > f->fcb->fcb.size_in_bytes){
              int surplus = f->pos_in_file + cnt - f->fcb->fcb.size_in_bytes;
              f->p_directory->fcb.size_in_bytes += surplus;
              f->fcb->fcb.size_in_bytes += surplus;
              DirectoryHandle* dh_tmp = SimpleFS_createDirHandle(f->p_directory, f->sfs);
              res = SimpleFS_updateSizeDir(dh_tmp, surplus, 1);
          }
          f->pos_in_file += cnt;    
          f->current_block = &(f->fcb->header); //ripristino
          res = DiskDriver_updateBlock(f->sfs->disk,f->fcb,f->fcb->fcb.block_in_disk);
          res = DiskDriver_updateBlock(f->sfs->disk,f->p_directory,f->p_directory->fcb.block_in_disk);

* int SimpleFS_read(FileHandle* f, void* data, int size);

      Reads in the file at the curret f->pos_in_file for 'size' and stores the read data in the buffer 'data'
      This function is pretty similar to the write, first we check where to start writing, if FirstFileBlock or the next, then we read navigating all the file blocks if necessary. pos_in_file is set to the bytes read 

          while(cnt < size && next > 0){
              toRead = (size - cnt)<max_fb?(size - cnt):max_fb;
              memcpy(aux, (fb_tmp->data+offset), toRead);
              res = DiskDriver_updateBlock(f->sfs->disk,read_block_file_tmp,next); //next contains block in disk where we're going to write
              cnt += toRead;
              aux += toRead;
              next = fb_tmp->header.next_block;
              if(cnt < size && next > 0){        
                  res = DiskDriver_readBlock(f->sfs->disk,read_block_file_tmp,next);
              }
              offset = 0;
          }
          f->pos_in_file += cnt;



* int SimpleFS_seek(FileHandle* f, int pos);

      Moves the pointer to the position 'pos' in the file and returns the number of byte read before arriving to pos

          if(pos < 0 || pos > f->fcb->fcb.size_in_bytes){
              printf("File too short or index invalid\n\n");
              return -1;
          }
          f->pos_in_file = pos;


* int SimpleFS_changeDir(DirectoryHandle* d, char* dirname);

      Functions that allows to navigate the directory structure inside our FS changing our DirectoryHandle to point to 'dirname'
          
          //We first check if the dirname is '..' and if a parent directory exist then if we're returning to root or not (change the p_directory field), then we proceed to change all the fields correctly. If the dirname is actually a name we need to search the desired folder and set a flag to 1 when found

          //Checking in the FirstDirectoryBlock
        
              for(i = 0; (i < idx_max_ffb) && !exist; i++){
                  idx_found = d->dcb->file_blocks[i];
                  res = DiskDriver_readBlock(d->sfs->disk, buffer_tmp, idx_found);
                
                  dirToRet_tmp = (FirstDirectoryBlock*)buffer_tmp;

                  if(strcmp(dirToRet_tmp->fcb.name,dirname) == 0 && dirToRet_tmp->fcb.is_dir == 1){
                      exist = 1;
                  }
              }
              
              // Then we check in the others DirectoryBlock 
              for(i = 1; (i < d->dcb->fcb.size_in_blocks) && !exist; i++){
                  int idx_block_dirb = d->current_block->next_block;
                  res = DiskDriver_readBlock(d->sfs->disk, dirb_tmp, idx_block_dirb);

                  db_tmp = (DirectoryBlock*)dirb_tmp; //last directory block
                  d->pos_in_block = idx_block_dirb; //id block n disk last db
                  d->current_block = &(db_tmp->header);//block header of last db

                  for(int j = 0; (j < idx_max_fb) && !exist; j++){
                        //SAME TO THE FIRST FOR
                  }
              }

* int SimpleFS_mkDir(DirectoryHandle* d, char* dirname);

      Creates a directory in the current one, checking if the folder already exist and if the new folder entry can be stored on the FirstDirectoryBlock or search for a free entry or allocate a new DirectoryBlock , then we can manage all the cases. Showing only how we create the new dir and how we modify the existing one

            d->dcb->num_entries += 1;
            memcpy(buffer_tmp, d->dcb, sizeof(FirstDirectoryBlock));
            res = DiskDriver_updateBlock(d->sfs->disk, buffer_tmp, d->dcb->fcb.block_in_disk);

            FirstDirectoryBlock fdb_tmp = {0};
            //NEW DIRECTORY FDB with fields correctly assigned
                
            memcpy(&(fdb_tmp.fcb.name), dirname, strlen(dirname));
            char fdb_block[BLOCK_SIZE] = {0};
            memcpy(fdb_block, &fdb_tmp, sizeof(FirstDirectoryBlock));
            
            res = DiskDriver_writeBlock(d->sfs->disk, fdb_block, fdb_tmp.fcb.block_in_disk);

* int SimpleFS_remove(DirectoryHandle* d, char* filename);

      THE most difficult function of the whole project cause of it's casuistry, so that we changed the declaration, the DirectoryHandle was a SimpleFS, but we found easier to manage in this way.
      Removes the file in the current directory and if it's a directory it removes recursively all the contained files and clears the bitmap for that blocks and modify accordingly the DiskHeader.

      This would be more verbose than a code highlights.
      We first check if the file exist and we set a flag to represent.In case the flag was not set we return -1.
      
      Then we need to make sure that our sizes in all the folder is coherent to the remove and that the file we want to remove it's actually a file; 
      If is a dir we read all the names and recursivelly call the remove on every name of files inside that folder:

          SimpleFS_readDir(names, dh_rec);
          for(i = 0; i < num_entries; i++) {
            SimpleFS_remove(dh_rec, names[i]);
          }
      
      When surpassed this phase we need to remove the empty folder (or the file) and that is managed through a series of if-else that consider the structure of the Directory to remove the file. It checks if the block to remove is in the last or intermediate block of the directory.
      We also re-arrange the array of entries to have always filled consecutives indeces.
      After we removed all of the data we set the BitMap


* int SimpleFS_syncCurrDir(DirectoryHandle* d);

      This is a support function created by us used when we want to update the directory handle parent 
        
          res = DiskDriver_readBlock(d->sfs->disk, tmp, d->dcb->fcb.block_in_disk);
          memcpy(d->dcb, tmp, BLOCK_SIZE);
          if(d->p_directory != NULL){
                res = DiskDriver_readBlock(d->sfs->disk, tmp, d->p_directory->fcb.block_in_disk);
                if(res < 0) return -1;
                memcpy(d->p_directory, tmp, BLOCK_SIZE);
          }
    
        

* int SimpleFS_updateSizeDir(DirectoryHandle* d, int size_in_bytes_torem, int addOrRemove);

      This is a support function create by us used when we needed to update the size of a folder (parent or not) when we write a file and effectively increase it's size.

      res = DiskDriver_readBlock(d->sfs->disk, tmp, d->dcb->fcb.block_in_disk);
      memcpy(d->dcb, tmp, BLOCK_SIZE);

      if(d->p_directory != NULL){
          res = DiskDriver_readBlock(d->sfs->disk, tmp, d->p_directory->fcb.block_in_disk);
          if(res < 0) return -1;
          memcpy(d->p_directory, tmp, BLOCK_SIZE);
      }

# Testing and benchmarks

We are now going to include in this documentation a couple of benchmarks to test out the efficiency of our File system, usable through the command:
    make benchmark

##Results

For this test we created a 2 Gigabyte disk then we start operating on that disk first with 2 large sized files then with 4190 files written for 500000 bytes each:

### 1st run:

    Disk formatted

    Creating 2 files sized about 1 gb each:

    Wrote first 1 GB in 36.818665 seconds
    Wrote second 1 GB in 33.752582 seconds

    Removing 2 gb worth of files...
    Removed 2 GB in 6.183746 seconds

    Creating writing and removing 4190 files:


    Created and written 4190 files in 252.467208 seconds

    Removed 4190 files in 14.244967 seconds

### 2nd run:
    
    Disk formatted

    Creating 2 files sized about 1 gb each:

    Wrote first 1 GB in 37.018860 seconds
    Wrote second 1 GB in 34.333659 seconds
    Removing 2 gb worth of files...

    Removed 2 GB in 6.109859 seconds

    Creating writing and removing 4190 files:
    Created and written 4190 files in 239.607080 seconds
    Removed 4190 files in 14.075854 seconds

### 3rd run:

    Disk formatted

    Creating 2 files sized about 1 gb each:

    Wrote first 1 GB in 35.109486 seconds
    Wrote second 1 GB in 35.231147 seconds
    Removing 2 gb worth of files...

    Removed 2 GB in 6.193434 seconds

    Creating writing and removing 4190 files:
    Created and written 4190 files in 243.667234 seconds 
    Removed 4190 files in 15.435576 seconds

