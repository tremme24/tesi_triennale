#include "simplefs.h"

DirectoryHandle* SimpleFS_init(SimpleFS* fs, DiskDriver* disk){
    if(!disk || !fs) return NULL;

    fs->disk = disk;

    if(fs->disk->header->first_free_block == fs->disk->header->bitmap_blocks){
        SimpleFS_format(fs);
        printf("Disk formatted\n\n");
    }
    else printf("Disk already formatted!\n\n");
    
    DirectoryHandle* directoryHandle = SimpleFS_directoryHandleAlloc();
    SimpleFS_directoryHandleInit(fs, directoryHandle, disk->header->bitmap_blocks); //I'm taking the root first directory block

    //set curr and root idx
    fs->root = directoryHandle->pos_in_block;
    fs->curr_dir = fs->root;

    return directoryHandle;
}

DirectoryHandle* SimpleFS_directoryHandleAlloc(){
    DirectoryHandle* directoryHandler = (DirectoryHandle*)malloc(sizeof(DirectoryHandle));
    if(!directoryHandler){
        printf("Error while allocating a DirectoryHandler struct\n\n");
        exit(-1);
    }

    directoryHandler->dcb = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
    if(!directoryHandler->dcb){
        printf("Error while allocating a FirstDirectoryBlock struct\n\n");
        exit(-1);
    }

    directoryHandler->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
    if(!directoryHandler->p_directory){
        printf("Error while allocating a FirstDirectoryBlock struct\n\n");
        exit(-1);
    }

    return directoryHandler;
}

void SimpleFS_directoryHandleInit(SimpleFS* sfs, DirectoryHandle* directoryHandle, int fdb_index){
    int res;

    directoryHandle->sfs = sfs;

    res = DiskDriver_readBlock(sfs->disk, directoryHandle->dcb, fdb_index);
    if(res < 0){
        printf("Cannot read first directory block of index = %d\n\n", fdb_index);
        exit(-1);
    }

    if(directoryHandle->dcb->fcb.directory_block < 0) {
        free(directoryHandle->p_directory);
        directoryHandle->p_directory = NULL;
    }
    else{
        if(!directoryHandle->p_directory) directoryHandle->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
        res = DiskDriver_readBlock(sfs->disk, directoryHandle->p_directory, directoryHandle->dcb->fcb.directory_block);
        if(res < 0){
            printf("Cannot read first directory block of parent on index = %d\n\n", directoryHandle->dcb->header.previous_block);
            exit(-1);
        }
    }

    directoryHandle->current_block = &(directoryHandle->dcb->header);
    directoryHandle->pos_in_dir = 0;
    directoryHandle->pos_in_block = fdb_index;
}

DirectoryBlock SimpleFS_directoryBlockInit(int previous_block, int block_in_file){
    DirectoryBlock directoryBlock = {0};

    directoryBlock.header.previous_block = previous_block;
    directoryBlock.header.next_block = -1;
    directoryBlock.header.block_in_file = block_in_file;

    memset(directoryBlock.file_blocks, 0, MAX_ENTRIES_IN_DB*sizeof(int));

    return directoryBlock;
}

FirstDirectoryBlock SimpleFS_firstDirectoryBlockInit(int p_directory_block, int block_in_disk, const char* name){
    FirstDirectoryBlock firstDirectoryBlock = {0};

    firstDirectoryBlock.header.previous_block = -1;
    firstDirectoryBlock.header.next_block = -1;
    firstDirectoryBlock.header.block_in_file = 0;

    firstDirectoryBlock.fcb.directory_block = p_directory_block;
    firstDirectoryBlock.fcb.block_in_disk = block_in_disk;
    firstDirectoryBlock.fcb.size_in_bytes = 0;
    firstDirectoryBlock.fcb.size_in_blocks = 1;
    firstDirectoryBlock.fcb.index_last_block = block_in_disk;
    firstDirectoryBlock.fcb.is_dir = 1;
    if(!strncpy(firstDirectoryBlock.fcb.name, name, 128)){
        printf("Error while copying name in first directory block of index = %d\n\n", block_in_disk);
        exit(-1);
    }

    firstDirectoryBlock.num_entries = 0;
    memset(firstDirectoryBlock.file_blocks, 0, MAX_ENTRIES_IN_FDB*sizeof(int));

    return firstDirectoryBlock;
}

FileBlock SimpleFS_fileBlockInit(int previous_block, int block_in_file){
    FileBlock fileBlock = {0};

    fileBlock.header.previous_block = previous_block;
    fileBlock.header.next_block = -1;
    fileBlock.header.block_in_file = block_in_file;

    memset(fileBlock.data, 0, MAX_ENTRIES_IN_DB*sizeof(char));

    return fileBlock;
}

FirstFileBlock SimpleFS_firstFileBlockInit(int p_directory_block, int block_in_disk, const char* name){
    FirstFileBlock firstFileBlock = {0};

    firstFileBlock.header.previous_block = -1;
    firstFileBlock.header.next_block = -1;
    firstFileBlock.header.block_in_file = 0;

    firstFileBlock.fcb.directory_block = p_directory_block;
    firstFileBlock.fcb.block_in_disk = block_in_disk;
    firstFileBlock.fcb.size_in_bytes = 0;
    firstFileBlock.fcb.size_in_blocks = 1;
    firstFileBlock.fcb.index_last_block = block_in_disk;
    firstFileBlock.fcb.is_dir = 0;
    if(!strncpy(firstFileBlock.fcb.name, name, 128)){
        printf("Error while copying name in first file block of index = %d\n\n", block_in_disk);
        exit(-1);
    }

    memset(firstFileBlock.data, 0, MAX_BYTE_IN_FFB);

    return firstFileBlock;
}

void SimpleFS_format(SimpleFS* fs){
    int i, res;

    //resetting bitmap
    for(i = fs->disk->header->bitmap_blocks; i < fs->disk->header->num_blocks; i++){
        res = DiskDriver_freeBlock(fs->disk, i);
        if(res < 0){
            printf("Cannot free block number %d when formatting disk\n\n", i);
            exit(-1);
        }
    }

    FirstDirectoryBlock root_fdb = SimpleFS_firstDirectoryBlockInit(-1, fs->disk->header->bitmap_blocks, "/");

    res = DiskDriver_writeBlock(fs->disk, &root_fdb, root_fdb.fcb.block_in_disk);
    if(res < 0){
        printf("Cannot create the '/' root directory\n\n");
        exit(-1);
    }    
}

FileHandle* SimpleFS_createFile(DirectoryHandle* d, const char* filename){
    int res, exists, idx_last_block_directory;
    char buffer_last_block_directory[BLOCK_SIZE] = {0};
    FirstDirectoryBlock* fdb_last_block_directory = (FirstDirectoryBlock*)buffer_last_block_directory;
    DirectoryBlock* db_last_block_directory = (DirectoryBlock*)buffer_last_block_directory;

    if(!d || !filename){
        printf("Error while passing parameters for create file\n\n");
        return NULL;
    }

    if(d->sfs->disk->header->first_free_block < 0){
        printf("No free blocks available\n\n");
        return NULL;
    };

    exists = SimpleFS_findFcbByNameInCurrentDirectory(d, filename, 1);
    if(exists > 0){
        printf("File %s already exist -> change name\n\n", filename);
        return NULL;
    }

    FirstFileBlock ffb_to_create =  SimpleFS_firstFileBlockInit(d->dcb->fcb.block_in_disk, 
    DiskDriver_getFreeBlock(d->sfs->disk, d->sfs->disk->header->first_free_block), filename);

    idx_last_block_directory = d->dcb->fcb.index_last_block;
    // printf("last block: %d\n\n", idx_last_block_directory);
    res = DiskDriver_readBlock(d->sfs->disk, fdb_last_block_directory, idx_last_block_directory);
    if(res < 0){
        printf("Error while reading last block of dir %s\n\n", d->dcb->fcb.name);
        exit(-1);
    }

    if(fdb_last_block_directory->header.previous_block < 0){
        //L'ultimo blocco è il FirstDirectoryBlock
        if(d->dcb->num_entries >= MAX_ENTRIES_IN_FDB){
            //Devo creare blocco nuovo della current directory, collegarlo e poi salvare nuova directory
            int idx_new_block_of_current_directory;
            idx_new_block_of_current_directory = SimpleFS_extendCurrentDirectory(d, fdb_last_block_directory, idx_last_block_directory);
            if(idx_new_block_of_current_directory < 0){
                printf("Error while extending current directory %s\n\n", d->dcb->fcb.name);
                return NULL;
            }
            
            //crearlo modificarlo e salvarlo
            DirectoryBlock db_to_extend = SimpleFS_directoryBlockInit(idx_last_block_directory, d->dcb->fcb.size_in_blocks);
            db_to_extend.file_blocks[0] = ffb_to_create.fcb.block_in_disk;

            res = DiskDriver_writeBlock(d->sfs->disk, &db_to_extend, idx_new_block_of_current_directory);
            if(res < 0){
                printf("Error while writing new block of current dir %s\n\n", d->dcb->fcb.name);
                return NULL;
            }

            res = DiskDriver_writeBlock(d->sfs->disk, &ffb_to_create, ffb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", filename);
                exit(-1);
            }
            
            d->dcb->fcb.index_last_block = idx_new_block_of_current_directory;
            d->dcb->num_entries += 1;
            d->dcb->fcb.size_in_blocks += 1;
            d->dcb->header.next_block = idx_new_block_of_current_directory;
        }
        else{
            res = DiskDriver_writeBlock(d->sfs->disk, &ffb_to_create, ffb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", filename);
                exit(-1);
            }

            d->dcb->file_blocks[d->dcb->num_entries] = ffb_to_create.fcb.block_in_disk;
            d->dcb->num_entries += 1;
        }

    }
    else{
        if(!((d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB))){
            //Ultimo blocco pieno
            int idx_new_block_of_current_directory;
            idx_new_block_of_current_directory = SimpleFS_extendCurrentDirectory(d, fdb_last_block_directory, idx_last_block_directory);
            if(idx_new_block_of_current_directory < 0){
                printf("Error while extending current directory %s\n\n", d->dcb->fcb.name);
                return NULL;
            }

            //crearlo modificarlo e salvarlo
            DirectoryBlock db_to_extend = SimpleFS_directoryBlockInit(idx_last_block_directory, d->dcb->fcb.size_in_blocks);
            db_to_extend.file_blocks[0] = ffb_to_create.fcb.block_in_disk;
            res = DiskDriver_writeBlock(d->sfs->disk, &db_to_extend, idx_new_block_of_current_directory);
            if(res < 0){
                printf("Error while writing new block of current dir %s\n\n", d->dcb->fcb.name);
                return NULL;
            }

            res = DiskDriver_writeBlock(d->sfs->disk, &ffb_to_create, ffb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", filename);
                exit(-1);
            }

            d->dcb->fcb.index_last_block = idx_new_block_of_current_directory;
            d->dcb->num_entries += 1;
            d->dcb->fcb.size_in_blocks += 1;

            //Salvo modifiche penultimo blocco directory corrente
            db_last_block_directory->header.next_block = idx_new_block_of_current_directory;
            res = DiskDriver_writeBlock(d->sfs->disk, db_last_block_directory, idx_last_block_directory);
            if(res < 0){
                printf("Error while updating on disk fdb of current dir %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }
        }
        else{
            //Ultimo blocco con una casella libera
            int idx_db_to_create_in_file_blocks = (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB);

            res = DiskDriver_writeBlock(d->sfs->disk, &ffb_to_create, ffb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", filename);
                exit(-1);
            }

            db_last_block_directory->file_blocks[idx_db_to_create_in_file_blocks] = ffb_to_create.fcb.block_in_disk;
            //Salvo modifiche all'ultimo blocco della directory corrente
            res = DiskDriver_writeBlock(d->sfs->disk, db_last_block_directory, idx_last_block_directory);
            if(res < 0){
                printf("Error while updating on disk last bloc of current dir %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }

            d->dcb->num_entries += 1;
        }
    }

    //Salvo modifiche alla directory corrente
    SimpleFS_saveDirectoryHandleOnDisk(d);

    FileHandle* fileHandle = SimpleFS_fileHandleAlloc();
    SimpleFS_fileHandleInit(d->sfs, d, fileHandle, ffb_to_create.fcb.block_in_disk);

    return fileHandle;
}

int SimpleFS_readDir(char** names, DirectoryHandle* d){
    int cnt = 0;
    int i, j, res, num_of_entries;
    char buffer_dir[BLOCK_SIZE] = {0};
    char buffer_file[BLOCK_SIZE] = {0};
    FirstDirectoryBlock* first_dir_block = (FirstDirectoryBlock*)(buffer_dir);
    DirectoryBlock* dir_block = (DirectoryBlock*)(buffer_dir);
    FirstFileBlock* first_file_block = (FirstFileBlock*)(buffer_file);

    for(i = 0; (i < d->dcb->fcb.size_in_blocks); i++){

        if(i == 0){
            num_of_entries = d->dcb->num_entries < MAX_ENTRIES_IN_FDB? d->dcb->num_entries : MAX_ENTRIES_IN_FDB;
            memcpy(first_dir_block, d->dcb, BLOCK_SIZE);
        }
        else{
            if(i == d->dcb->fcb.size_in_blocks - 1 && (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB)) num_of_entries = (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB);
            else num_of_entries = MAX_ENTRIES_IN_DB;

            res = DiskDriver_readBlock(d->sfs->disk, dir_block, d->current_block->next_block);
            if(res < 0){
                printf("Error when reading first block of file/dir\n\n");
                return -1;
            }

            d->current_block = &(dir_block->header);
        }

        // printf("num_of_entries: %d\n\n", num_of_entries);
        for(j = 0; (j < num_of_entries); j++, cnt++){
            
            if(i == 0) res = DiskDriver_readBlock(d->sfs->disk, first_file_block, first_dir_block->file_blocks[j]);
            else res = DiskDriver_readBlock(d->sfs->disk, first_file_block, dir_block->file_blocks[j]);

            if(res < 0){
                // printf("num entries = %d, j = %d\n\n", num_of_entries, j);
                printf("Error when reading first block of file/dir asssss\n\n");
                return -1;
            }
            
            if(!strncpy(names[cnt], first_file_block->fcb.name, 128)){
                printf("Error when reading name of file\n\n");
                return -1;
            }
        }
    }

    d->current_block = &(d->dcb->header);

    return cnt;
}

FileHandle* SimpleFS_fileHandleAlloc(){
    FileHandle* fileHandle = (FileHandle*)malloc(sizeof(FileHandle));
    if(!fileHandle){
        printf("Error while allocating a FileHandler struct\n\n");
        exit(-1);
    }

    fileHandle->fcb = (FirstFileBlock*)malloc(sizeof(FirstFileBlock));
    if(!fileHandle->fcb){
        printf("Error while allocating a FirstFileBlock struct\n\n");
        exit(-1);
    }

    fileHandle->p_directory = (FirstDirectoryBlock*)malloc(sizeof(FirstDirectoryBlock));
    if(!fileHandle->p_directory){
        printf("Error while allocating a FirstFileBlock struct\n\n");
        exit(-1);
    }

    return fileHandle;
}

void SimpleFS_fileHandleInit(SimpleFS* sfs, DirectoryHandle* directoryHandle, FileHandle* fileHandle, int ffb_index){
    int res;

    fileHandle->sfs = sfs;

    res = DiskDriver_readBlock(sfs->disk, fileHandle->fcb, ffb_index);
    if(res < 0){
        printf("Cannot read first file block of index = %d\n\n", ffb_index);
        exit(-1);
    }

    res = DiskDriver_readBlock(sfs->disk, fileHandle->p_directory, directoryHandle->dcb->fcb.block_in_disk);
    if(res < 0){
        printf("Cannot read first directory block of parent on index = %d\n\n", directoryHandle->dcb->fcb.block_in_disk);
        exit(-1);
    }

    fileHandle->current_block = &(fileHandle->fcb->header);
    fileHandle->pos_in_file = 0;
}

FileHandle* SimpleFS_openFile(DirectoryHandle* d, const char* filename){
    if(!d || !filename){
        printf("Error while passing parameters in open file\n\n");
        return NULL;
    }

    int idx_file_to_open = SimpleFS_findFcbByNameInCurrentDirectory(d, filename, 0);
    if(idx_file_to_open < 0){
        printf("Error file doesn't exist\n\n");
        return NULL;
    }

    FileHandle* fileHandle = SimpleFS_fileHandleAlloc();
    SimpleFS_fileHandleInit(d->sfs, d, fileHandle, idx_file_to_open);

    return fileHandle;
}

int SimpleFS_close(FileHandle* f){
    if(!f) return 0;
    free(f->p_directory);
    free(f->fcb);
    free(f);

    return 0;
}

int SimpleFS_findBlockFileByBlockInFile(FileHandle* f, int block_in_file){
    int res, i;
    int index_to_return = f->fcb->fcb.block_in_disk;
    char buffer_file[BLOCK_SIZE] = {0};
    FileBlock* file_block = (FileBlock*)(buffer_file);

    if(block_in_file > (f->fcb->fcb.size_in_blocks - 1)) return -1;

    for(i = 0; i < block_in_file ; i++){
        index_to_return = f->current_block->next_block;
        res = DiskDriver_readBlock(f->sfs->disk, file_block, f->current_block->next_block);
        if(res < 0){
            printf("Error when reading %d block of file %s\n\n", f->current_block->next_block, f->fcb->fcb.name);
            return -1;
        }

        f->current_block = &(file_block->header);
    }

    f->current_block = &(f->fcb->header);

    return index_to_return;
}

int SimpleFS_findBlockDirectoryByBlockInFile(DirectoryHandle* d, int block_in_dir){
    int res, i;
    int index_to_return = d->dcb->fcb.block_in_disk;
    char buffer_directory[BLOCK_SIZE] = {0};
    DirectoryBlock* directory_block = (DirectoryBlock*)(buffer_directory);

    if(block_in_dir > (d->dcb->fcb.size_in_blocks - 1)) return -1;

    for(i = 0; i < block_in_dir ; i++){
        index_to_return = d->current_block->next_block;
        res = DiskDriver_readBlock(d->sfs->disk, directory_block, d->current_block->next_block);
        if(res < 0){
            printf("Error when reading %d block of directory %s\n\n", d->current_block->next_block, d->dcb->fcb.name);
            return -1;
        }

        d->current_block = &(directory_block->header);
    }

    d->current_block = &(d->dcb->header);

    return index_to_return;
}

int SimpleFS_extendCurrentFile(FileHandle* f, int n_file_blocks_to_add){
    int res, i, idx_new_block_of_current_file, idx_last_block_file;
    FirstFileBlock ffb_last_block_file = {0};
    FileBlock fb_to_create = {0};

    idx_last_block_file = f->fcb->fcb.index_last_block;
    if(idx_last_block_file < 0){
        printf("Error while finding last block of file %s\n\n", f->fcb->fcb.name);
        return -1;
    }

    res = DiskDriver_readBlock(f->sfs->disk, &ffb_last_block_file, idx_last_block_file);
    if(res < 0){
        printf("Error while reading last block of file %s\n\n", f->fcb->fcb.name);
        exit(-1);
    }

    for(i = 0; i < n_file_blocks_to_add; i++){
        idx_new_block_of_current_file = DiskDriver_getFreeBlock(f->sfs->disk, f->sfs->disk->header->first_free_block);

        if(ffb_last_block_file.header.previous_block < 0) f->fcb->header.next_block = idx_new_block_of_current_file; 
        ffb_last_block_file.header.next_block = idx_new_block_of_current_file;
        res = DiskDriver_writeBlock(f->sfs->disk, &ffb_last_block_file, idx_last_block_file);
        if (res < 0){
            printf("Error while updating last block of current dir %s\n\n", f->fcb->fcb.name);
            return -1;
        }

        fb_to_create = SimpleFS_fileBlockInit(idx_last_block_file, f->fcb->fcb.size_in_blocks);
        res = DiskDriver_writeBlock(f->sfs->disk, &fb_to_create, idx_new_block_of_current_file);
        if (res < 0){
            printf("Error while updating last block of current dir %s\n\n", f->fcb->fcb.name);
            return -1;
        }

        idx_last_block_file = idx_new_block_of_current_file;
        memcpy(&ffb_last_block_file, &fb_to_create, BLOCK_SIZE);
        f->fcb->fcb.size_in_blocks += 1;
    }

    f->fcb->fcb.index_last_block = idx_last_block_file;

    return 0;
}

int SimpleFS_write(FileHandle* f, void* data, int size){
    int i, res, n_file_blocks_to_add, block_in_file,
        index_block_file_start_writing, byte_to_write, offset,
        index_current_block_file;
    int starting_offset = f->pos_in_file;
    int starting_size_in_bytes = f->fcb->fcb.size_in_bytes;
    int byte_wrote = 0;
    int bytes_surplus = 0;
    int file_bytes_available = MAX_BYTE_IN_FFB + MAX_BYTE_IN_FB*(f->fcb->fcb.size_in_blocks - 1);
    char buffer_file[BLOCK_SIZE] = {0};
    FileBlock* file_block = (FileBlock*)(buffer_file);

    if(!f || !data || size < 0 || size == 0){
        printf("Error while passing parameters write\n\n");
        return -1;
    }

    //I have to add file blocks
    if(size > (file_bytes_available - f->pos_in_file)){
        bytes_surplus = size - (file_bytes_available - f->pos_in_file);
        n_file_blocks_to_add = bytes_surplus % MAX_BYTE_IN_FB? (bytes_surplus / MAX_BYTE_IN_FB) + 1 : (bytes_surplus / MAX_BYTE_IN_FB);
        // printf("Blocks requested %d\n\n", n_file_blocks_to_add);

        if(n_file_blocks_to_add > f->sfs->disk->header->free_blocks){
            printf("You're trying to write more than free space on disk -> writing 'till fill the disk\n\n");
            n_file_blocks_to_add = f->sfs->disk->header->free_blocks;
            bytes_surplus = f->sfs->disk->header->free_blocks * MAX_BYTE_IN_FB;
            size = (file_bytes_available - f->pos_in_file) + bytes_surplus;
        }

        res = SimpleFS_extendCurrentFile(f, n_file_blocks_to_add);
        if(res < 0){
            printf("Error while extending current file %s\n\n", f->fcb->fcb.name);
            exit(-1);
        }

        // printf("Added %d file blocks\n\n", n_file_blocks_to_add);
        // printDisk(f->sfs->disk);

    }

    //find block in file index where to start writing
    if(f->pos_in_file < MAX_BYTE_IN_FFB) block_in_file = 0;
    else block_in_file = ((f->pos_in_file - MAX_BYTE_IN_FFB) / MAX_BYTE_IN_FB) + 1;

    //find and read block of file on disk where to start reading
    index_block_file_start_writing = SimpleFS_findBlockFileByBlockInFile(f, block_in_file);
    if(index_block_file_start_writing < 0){
        printf("Error while finding index on disk of block file where to start writing of file %s\n\n", f->fcb->fcb.name);
        exit(-1);
    }

    res = DiskDriver_readBlock(f->sfs->disk, buffer_file, index_block_file_start_writing);
    if(res < 0){
        printf("Error while reading block file where to start writing of file %s\n\n", f->fcb->fcb.name);
        exit(-1);
    }

    index_current_block_file = index_block_file_start_writing;
    f->current_block = &(file_block->header);

    for(i = block_in_file; (i < f->fcb->fcb.size_in_blocks) && (byte_wrote < size); i++){

        // printf("Iterazione numero %d\n\n", i);

        if(i == 0){
            offset = f->pos_in_file;
            byte_to_write = (size - byte_wrote) < (MAX_BYTE_IN_FFB - offset)? (size - byte_wrote) : (MAX_BYTE_IN_FFB - offset);
            memcpy(f->fcb->data + offset, data + byte_wrote, byte_to_write);
        }
        else{
            offset = (f->pos_in_file - MAX_BYTE_IN_FFB) % MAX_BYTE_IN_FB;
            byte_to_write = (size - byte_wrote) < (MAX_BYTE_IN_FB - offset)? (size - byte_wrote) : (MAX_BYTE_IN_FB - offset);
            memcpy(file_block->data + offset, data + byte_wrote, byte_to_write);

            res = DiskDriver_writeBlock(f->sfs->disk, buffer_file, index_current_block_file);
            if(res < 0){
                printf("Error while updating data in block %d of file %s\n\n", index_current_block_file, f->fcb->fcb.name);
                exit(-1);
            }
        }

        byte_wrote += byte_to_write;
        f->pos_in_file += byte_to_write;

        // printf("Offset = %d\n\n", offset);
        // printf("byte_to_write = %d\n\n", byte_to_write);

        //lettura preventiva prox blocco (safe)
        if(f->current_block->next_block > 0){
            index_current_block_file = f->current_block->next_block;
            res = DiskDriver_readBlock(f->sfs->disk, file_block, f->current_block->next_block);
            if(res < 0){
                printf("Error while reading %d block of file %s\n\n", f->current_block->next_block, f->fcb->fcb.name);
                return -1;
            }
        }
    }
    f->current_block = &(f->fcb->header);

    if(f->fcb->fcb.size_in_bytes < byte_wrote + starting_offset){
        f->fcb->fcb.size_in_bytes = byte_wrote + starting_offset;
        DirectoryHandle* d = SimpleFS_directoryHandleAlloc();
        SimpleFS_directoryHandleInit(f->sfs, d, f->p_directory->fcb.block_in_disk);
        SimpleFS_updateSizeDirectory(d, f->fcb->fcb.size_in_bytes - starting_size_in_bytes, 1);
        free(d->p_directory);
        free(d->dcb);
        free(d);
    }

    //updating first file block modified
    res = DiskDriver_writeBlock(f->sfs->disk, f->fcb, f->fcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error while updating first file block of file %s\n\n", f->fcb->fcb.name);
        return -1;
    }

    return byte_wrote;
}

int SimpleFS_read(FileHandle* f, void* data, int size){
    int i, res, block_in_file, index_block_file_start_reading, byte_to_read, offset;
    int byte_read = 0;
    char buffer_file[BLOCK_SIZE] = {0};
    FirstFileBlock* first_file_block = (FirstFileBlock*)(buffer_file);
    FileBlock* file_block = (FileBlock*)(buffer_file);

    if(!f || !data || size < 0 || size == 0){
        printf("Error while passing parameters read\n\n");
        return -1;
    }

    if(!f->fcb->fcb.size_in_bytes){
        printf("File %s is empty\n\n", f->fcb->fcb.name);
        return -1;
    }

    if((f->pos_in_file + size) > f->fcb->fcb.size_in_bytes) {
        printf("You're trying to read more than size of file %s --> Reading 'till the EOF\n\n", f->fcb->fcb.name);
        size = f->fcb->fcb.size_in_bytes - f->pos_in_file;
    }

    //find block in file index where to start reading
    if(f->pos_in_file < MAX_BYTE_IN_FFB) block_in_file = 0;
    else block_in_file = ((f->pos_in_file - MAX_BYTE_IN_FFB) / MAX_BYTE_IN_FB) + 1;

    // printf("Starting from %d\n\n", block_in_file);

    //find and read block of file on disk where to start reading
    index_block_file_start_reading = SimpleFS_findBlockFileByBlockInFile(f, block_in_file);
    if(index_block_file_start_reading < 0){
        printf("Error while finding index on disk of block file where to start reading of file %s\n\n", f->fcb->fcb.name);
        exit(-1);
    }

    res = DiskDriver_readBlock(f->sfs->disk, buffer_file, index_block_file_start_reading);
    if(res < 0){
        printf("Error while reading block file where to start reading of file %s\n\n", f->fcb->fcb.name);
        exit(-1);
    }

    f->current_block = &(file_block->header);

    for(i = block_in_file; (i < f->fcb->fcb.size_in_blocks) && (byte_read < size); i++){

        if(i == 0){
            offset = f->pos_in_file;
            byte_to_read = (size - byte_read) < (MAX_BYTE_IN_FFB - offset)? (size - byte_read) : (MAX_BYTE_IN_FFB - offset);
            memcpy(data + byte_read, first_file_block->data + offset, byte_to_read);
        }
        else{
            offset = (f->pos_in_file - MAX_BYTE_IN_FFB) % MAX_BYTE_IN_FB;
            byte_to_read = (size - byte_read) < (MAX_BYTE_IN_FB - offset)? (size - byte_read) : (MAX_BYTE_IN_FB - offset);
            memcpy(data + byte_read, file_block->data + offset, byte_to_read);
        }

        // printf("Iteration number %d\n\n", i);
        // printf("byte_to_read = %d\n\n", byte_to_read);
        // printf("offset = %d\n\n", offset);

        byte_read += byte_to_read;
        f->pos_in_file += byte_to_read;
        
        //lettura preventiva prox blocco (safe)
        if(f->current_block->next_block > 0){
            res = DiskDriver_readBlock(f->sfs->disk, file_block, f->current_block->next_block);
            if(res < 0){
                printf("Error when reading %d block of file %s\n\n", f->current_block->next_block, f->fcb->fcb.name);
                return -1;
            }
        }
    }

    f->current_block = &(f->fcb->header);

    return byte_read;
}

int SimpleFS_seek(FileHandle* f, int pos){
    if(!f || pos < 0){
        printf("Error while passing parameters seek\n\n");
        return -1;
    }

    if(pos > f->fcb->fcb.size_in_bytes){
        printf("File too short or index invalid\n\n");
        return -1;
    }

    f->pos_in_file = pos;

    return pos;
}

int SimpleFS_findFcbByNameInCurrentDirectory(DirectoryHandle* d, const char* name, int is_dir){
    int res, num_of_entries, i, j;
    int index_to_return = -1;
    char buffer_dir[BLOCK_SIZE] = {0};
    char buffer_file[BLOCK_SIZE] = {0};
    FirstDirectoryBlock* first_dir_block = (FirstDirectoryBlock*)(buffer_dir);
    DirectoryBlock* dir_block = (DirectoryBlock*)(buffer_dir);
    FirstFileBlock* first_file_block = (FirstFileBlock*)(buffer_file);

    for(i = 0; (i < d->dcb->fcb.size_in_blocks) && (index_to_return < 0) ; i++){

        if(i == 0){
            num_of_entries = d->dcb->num_entries < MAX_ENTRIES_IN_FDB? d->dcb->num_entries : MAX_ENTRIES_IN_FDB;
            memcpy(first_dir_block, d->dcb, BLOCK_SIZE);
        }
        else{
            if(i == d->dcb->fcb.size_in_blocks - 1 && (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB)) num_of_entries = (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB);
            else num_of_entries = MAX_ENTRIES_IN_DB;

            res = DiskDriver_readBlock(d->sfs->disk, dir_block, d->current_block->next_block);
            if(res < 0){
                printf("Error when reading first block of file/dir\n\n");
                return -1;
            }

            d->current_block = &(dir_block->header);
        }

        for(j = 0; (j < num_of_entries) && (index_to_return < 0); j++){
            
            if(i == 0) res = DiskDriver_readBlock(d->sfs->disk, first_file_block, first_dir_block->file_blocks[j]);
            else res = DiskDriver_readBlock(d->sfs->disk, first_file_block, dir_block->file_blocks[j]);

            if(res < 0){
                printf("Error when reading first block of file/dir asssss 1\n\n");
                return -1;
            }

            if(strncmp(first_file_block->fcb.name, name, 128) == 0 && first_file_block->fcb.is_dir == is_dir){
                index_to_return = first_file_block->fcb.block_in_disk;
            }
        }
    }

    d->current_block = &(d->dcb->header);

    return index_to_return;
}

int SimpleFS_changeDir(DirectoryHandle* d, char* dirname){
    int idx_found;

    if(!d || !dirname){
        printf("Error while passing parameters for change directory\n\n");
        return -1;
    }
    
    if(strcmp(dirname, "..") == 0){
        if(d->p_directory == NULL){ //check if it has a p_dir
            printf("No parent dir, are you on root?\n\n");
            return -1;
        }

        else{
            SimpleFS_directoryHandleInit(d->sfs, d, d->p_directory->fcb.block_in_disk);
        }

        return 0;
    }

    idx_found = SimpleFS_findFcbByNameInCurrentDirectory(d, dirname, 1);
    if(idx_found < 0){
        return -1;
    }

    SimpleFS_directoryHandleInit(d->sfs, d, idx_found);
    
    return 0;
}

int SimpleFS_extendCurrentDirectory(DirectoryHandle* d, FirstDirectoryBlock* fdb_last_block_directory, int idx_last_block_directory){
    int res;

    int idx_new_block_of_current_directory;
    idx_new_block_of_current_directory = DiskDriver_getFreeBlock(d->sfs->disk, d->sfs->disk->header->first_free_block + 1);

    if(idx_new_block_of_current_directory < 0){
        printf("No free blocks available for extend current directory\n\n");
        return -1;
    }

    fdb_last_block_directory->header.next_block = idx_new_block_of_current_directory;
    res = DiskDriver_writeBlock(d->sfs->disk, fdb_last_block_directory, idx_last_block_directory);
    if (res < 0){
        printf("Error while updating last block of current dir %s\n\n", d->dcb->fcb.name);
        return -1;
    }

    return idx_new_block_of_current_directory;
}

int SimpleFS_reduceCurrentDirectory(DirectoryHandle* d, FirstDirectoryBlock* fdb_last_block_directory, int idx_last_block_directory){
    int res;
    int idx_previous_block_of_current_directory;
    char previous_block_buffer[BLOCK_SIZE] = {0};
    FirstDirectoryBlock* fdb_previous = (FirstDirectoryBlock*)previous_block_buffer;

    idx_previous_block_of_current_directory = fdb_last_block_directory->header.previous_block;
    // printf("Index previous to delete: %d\n\n", idx_previous_block_of_current_directory);

    res = DiskDriver_readBlock(d->sfs->disk, fdb_previous, idx_previous_block_of_current_directory);
    if (res < 0){
        printf("Error while reading previous block of current dir %s\n\n", d->dcb->fcb.name);
        return -1;
    }

    // printf("Previous del previous: %d\n\n", fdb_previous->header.previous_block);
    if(fdb_previous->header.previous_block < 0) d->dcb->header.next_block = -1;
    else{
        fdb_previous->header.next_block = -1;
        res = DiskDriver_writeBlock(d->sfs->disk, fdb_previous, idx_previous_block_of_current_directory);
        if (res < 0){
            printf("Error while updating previous block of current dir %s\n\n", d->dcb->fcb.name);
            return -1;
        }
    }

    d->dcb->fcb.index_last_block = idx_previous_block_of_current_directory;

    return 0;
}

int SimpleFS_mkDir(DirectoryHandle* d, char* dirname){
    int res, exists, idx_last_block_directory;
    char buffer_last_block_directory[BLOCK_SIZE] = {0};
    FirstDirectoryBlock* fdb_last_block_directory = (FirstDirectoryBlock*)buffer_last_block_directory;
    DirectoryBlock* db_last_block_directory = (DirectoryBlock*)buffer_last_block_directory;

    if(!d || !dirname){
        printf("Error while passing parameters for make directory\n\n");
        return -1;
    }

    if(d->sfs->disk->header->first_free_block < 0){
        printf("No free blocks available\n\n");
        return -1;
    };

    exists = SimpleFS_findFcbByNameInCurrentDirectory(d, dirname, 1);
    if(exists > 0){
        printf("Directory %s already exist -> change name\n\n", dirname);
        return -1;
    }

    FirstDirectoryBlock fdb_to_create =  SimpleFS_firstDirectoryBlockInit(d->dcb->fcb.block_in_disk, 
    DiskDriver_getFreeBlock(d->sfs->disk, d->sfs->disk->header->first_free_block), dirname);

    idx_last_block_directory = d->dcb->fcb.index_last_block;
    res = DiskDriver_readBlock(d->sfs->disk, fdb_last_block_directory, idx_last_block_directory);
    if(res < 0){
        printf("Error while reading last block of dir %s\n\n", d->dcb->fcb.name);
        exit(-1);
    }

    if(fdb_last_block_directory->header.previous_block < 0){
        //L'ultimo blocco è il FirstDirectoryBlock
        if(d->dcb->num_entries >= MAX_ENTRIES_IN_FDB){
            //Devo creare blocco nuovo della current directory, collegarlo e poi salvare nuova directory
            int idx_new_block_of_current_directory;
            idx_new_block_of_current_directory = SimpleFS_extendCurrentDirectory(d, fdb_last_block_directory, idx_last_block_directory);
            if(idx_new_block_of_current_directory < 0){
                printf("Error while extending current directory %s\n\n", d->dcb->fcb.name);
                return -1;
            }
            
            //crearlo modificarlo e salvarlo
            DirectoryBlock db_to_extend = SimpleFS_directoryBlockInit(idx_last_block_directory, d->dcb->fcb.size_in_blocks);
            db_to_extend.file_blocks[0] = fdb_to_create.fcb.block_in_disk;

            res = DiskDriver_writeBlock(d->sfs->disk, &db_to_extend, idx_new_block_of_current_directory);
            if(res < 0){
                printf("Error while writing new block of current dir %s\n\n", d->dcb->fcb.name);
                return -1;
            }

            res = DiskDriver_writeBlock(d->sfs->disk, &fdb_to_create, fdb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", dirname);
                exit(-1);
            }

            d->dcb->fcb.index_last_block = idx_new_block_of_current_directory;
            d->dcb->num_entries += 1;
            d->dcb->fcb.size_in_blocks += 1;
            d->dcb->header.next_block = idx_new_block_of_current_directory;
        }
        else{
            res = DiskDriver_writeBlock(d->sfs->disk, &fdb_to_create, fdb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", dirname);
                exit(-1);
            }

            d->dcb->file_blocks[d->dcb->num_entries] = fdb_to_create.fcb.block_in_disk;
            d->dcb->num_entries += 1;
        }

    }
    else{
        if(!((d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB))){
            //Ultimo blocco pieno
            int idx_new_block_of_current_directory;
            idx_new_block_of_current_directory = SimpleFS_extendCurrentDirectory(d, fdb_last_block_directory, idx_last_block_directory);
            if(idx_new_block_of_current_directory < 0){
                printf("Error while extending current directory %s\n\n", d->dcb->fcb.name);
                return -1;
            }

            //crearlo modificarlo e salvarlo
            DirectoryBlock db_to_extend = SimpleFS_directoryBlockInit(idx_last_block_directory, d->dcb->fcb.size_in_blocks);
            db_to_extend.file_blocks[0] = fdb_to_create.fcb.block_in_disk;
            res = DiskDriver_writeBlock(d->sfs->disk, &db_to_extend, idx_new_block_of_current_directory);
            if(res < 0){
                printf("Error while writing new block of current dir %s\n\n", d->dcb->fcb.name);
                return -1;
            }

            res = DiskDriver_writeBlock(d->sfs->disk, &fdb_to_create, fdb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", dirname);
                exit(-1);
            }

            d->dcb->fcb.index_last_block = idx_new_block_of_current_directory;
            d->dcb->num_entries += 1;
            d->dcb->fcb.size_in_blocks += 1;

            //Salvo modifiche penultimo blocco directory corrente
            db_last_block_directory->header.next_block = idx_new_block_of_current_directory;
            res = DiskDriver_writeBlock(d->sfs->disk, db_last_block_directory, idx_last_block_directory);
            if(res < 0){
                printf("Error while updating on disk fdb of current dir %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }
        }
        else{
            //Ultimo blocco con una casella libera
            int idx_db_to_create_in_file_blocks = (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB);

            res = DiskDriver_writeBlock(d->sfs->disk, &fdb_to_create, fdb_to_create.fcb.block_in_disk);
            if(res < 0){
                printf("Error while writing on disk fdb of dir to create %s\n\n", dirname);
                exit(-1);
            }

            db_last_block_directory->file_blocks[idx_db_to_create_in_file_blocks] = fdb_to_create.fcb.block_in_disk;
            //Salvo modifiche all'ultimo blocco della directory corrente
            res = DiskDriver_writeBlock(d->sfs->disk, db_last_block_directory, idx_last_block_directory);
            if(res < 0){
                printf("Error while updating on disk last bloc of current dir %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }

            d->dcb->num_entries += 1;
        }
    }

    //Salvo modifiche alla directory corrente
    SimpleFS_saveDirectoryHandleOnDisk(d);

    return 0;
}

int SimpleFS_updateSizeDirectory(DirectoryHandle* d, int size_in_bytes_to_remove, int add_or_remove){
    int res;
    FirstDirectoryBlock fdb_dir_to_upload = {0};

    if(add_or_remove == 0) d->dcb->fcb.size_in_bytes -= size_in_bytes_to_remove; // aggiorno padre e vado a prendere gli altri padri senza cambiare dcb
    else d->dcb->fcb.size_in_bytes += size_in_bytes_to_remove;

    res = DiskDriver_writeBlock(d->sfs->disk, d->dcb, d->dcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error while uploading current directory\n\n");
        return -1;
    }

    if(d->p_directory == NULL) return 0;

    memcpy(&fdb_dir_to_upload, d->p_directory, sizeof(FirstDirectoryBlock));
    
    if(add_or_remove == 0) fdb_dir_to_upload.fcb.size_in_bytes -= size_in_bytes_to_remove; // aggiorno padre e vado a prendere gli altri padri senza cambiare dcb
    else fdb_dir_to_upload.fcb.size_in_bytes += size_in_bytes_to_remove;

    res = DiskDriver_writeBlock(d->sfs->disk, &fdb_dir_to_upload, fdb_dir_to_upload.fcb.block_in_disk);
    if(res < 0){
        printf("Error while uploading parent directory\n\n");
        return -1;
    }

    while(fdb_dir_to_upload.fcb.directory_block > 0){
        res = DiskDriver_readBlock(d->sfs->disk, &fdb_dir_to_upload, fdb_dir_to_upload.fcb.directory_block);
        if(res < 0) {
            printf("Error while reading parent directory\n\n");
            return -1;
        }

        if(add_or_remove == 0) fdb_dir_to_upload.fcb.size_in_bytes -= size_in_bytes_to_remove;
        else fdb_dir_to_upload.fcb.size_in_bytes += size_in_bytes_to_remove;

        res = DiskDriver_writeBlock(d->sfs->disk, &fdb_dir_to_upload, fdb_dir_to_upload.fcb.block_in_disk);
        if(res < 0) {
            printf("Error while writing parent directory\n\n");
            return -1;
        }
    }

    return 0;
}

IndexesEntryOfFile SimpleFS_findForRemove(DirectoryHandle* d, const char* name){
    int res, num_of_entries, i, j;
    IndexesEntryOfFile index_entry_file_to_return = {-1, -1, -1};
    char buffer_dir[BLOCK_SIZE] = {0};
    char buffer_file[BLOCK_SIZE] = {0};
    FirstDirectoryBlock* first_dir_block = (FirstDirectoryBlock*)(buffer_dir);
    DirectoryBlock* dir_block = (DirectoryBlock*)(buffer_dir);
    FirstFileBlock* first_file_block = (FirstFileBlock*)(buffer_file);

    for(i = 0; (i < d->dcb->fcb.size_in_blocks) && (index_entry_file_to_return.index_on_disk < 0) ; i++){

        if(i == 0){
            num_of_entries = d->dcb->num_entries < MAX_ENTRIES_IN_FDB? d->dcb->num_entries : MAX_ENTRIES_IN_FDB;
            memcpy(first_dir_block, d->dcb, BLOCK_SIZE);
        }
        else{
            if(i == d->dcb->fcb.size_in_blocks - 1 && (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB)) num_of_entries = (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB);
            else num_of_entries = MAX_ENTRIES_IN_DB;

            res = DiskDriver_readBlock(d->sfs->disk, dir_block, d->current_block->next_block);
            if(res < 0){
                printf("Error when reading first block of file/dir\n\n");
                exit(-1);
            }

            d->current_block = &(dir_block->header);
        }

        for(j = 0; (j < num_of_entries) && (index_entry_file_to_return.index_on_disk < 0); j++){
            
            if(i == 0) res = DiskDriver_readBlock(d->sfs->disk, first_file_block, first_dir_block->file_blocks[j]);
            else res = DiskDriver_readBlock(d->sfs->disk, first_file_block, dir_block->file_blocks[j]);

            if(res < 0){
                printf("Error when reading first block of file/dir asssss 1\n\n");
                exit(-1);
            }

            if(strncmp(first_file_block->fcb.name, name, 128) == 0){
                index_entry_file_to_return.index_on_disk = first_file_block->fcb.block_in_disk;
                index_entry_file_to_return.entry = j;
                index_entry_file_to_return.index_relative_of_block_directory = i;
            }
        }
    }

    d->current_block = &(d->dcb->header);
    return index_entry_file_to_return;
}

int SimpleFS_freeAllBlocksOfFile(DirectoryHandle* d, FirstFileBlock* ffb_to_remove){
    int i, res, index_current_block_file; 
    index_current_block_file = ffb_to_remove->fcb.block_in_disk;

    for(i = 0; (i < ffb_to_remove->fcb.size_in_blocks) && (index_current_block_file > 0); i++){
        res = DiskDriver_freeBlock(d->sfs->disk, index_current_block_file);
        if(res < 0){
            printf("Error while freeing %d block of file/dir %s\n\n", i, ffb_to_remove->fcb.name);
            return -1;
        }

        index_current_block_file = ffb_to_remove->header.next_block;
        if(index_current_block_file > 0){
            res = DiskDriver_readBlock(d->sfs->disk, ffb_to_remove, ffb_to_remove->header.next_block);
            if(res < 0){
                printf("Error while reading %d block of file/dir %s\n\n", i, ffb_to_remove->fcb.name);
                return -1;
            }
        }
    }

    return 0;
}

int SimpleFS_remove(DirectoryHandle* d, char* filename){
    int res, i, index_last_block_current_directory, n_entries_last_block_current_directory, index_of_block_middle;
    IndexesEntryOfFile index_entry_file = {0};
    FirstFileBlock ffb_to_remove = {0};
    char last_block_current_directory_buffer[BLOCK_SIZE] = {0};
    DirectoryBlock* db_last_block_current_directory = (DirectoryBlock*)last_block_current_directory_buffer;

    if(!d || !filename){
        printf("Error while passing parameters remove\n\n");
        return -1;
    }

    //finding index and entry of file/dir to remove
    index_entry_file = SimpleFS_findForRemove(d, filename);
    if(index_entry_file.index_on_disk < 0){
        printf("File or directory doesn't exist\n\n");
        return -1;
    }

    // printf("index on disk: %d, index relative: %d, entry:%d\n\n", index_entry_file.index_on_disk, index_entry_file.index_relative_of_block_directory, index_entry_file.entry);
    //reading the FirstFileBlock to remove
    res = DiskDriver_readBlock(d->sfs->disk, &ffb_to_remove, index_entry_file.index_on_disk);
    if(res < 0){
        printf("Error while reading first file block to remove of file %s\n\n", ffb_to_remove.fcb.name);
        exit(-1);
    }

    //updating size of current dir and parents
    SimpleFS_updateSizeDirectory(d, ffb_to_remove.fcb.size_in_bytes, 0);

    //if a dir to remove, use recursion to delete all the content
    if(ffb_to_remove.fcb.is_dir) {
        // printf("Is a dir!!!\n\n");
        FirstDirectoryBlock* fdb_to_remove = (FirstDirectoryBlock*)&ffb_to_remove;

        char** names = malloc(sizeof(char*)*(fdb_to_remove->num_entries));
        for(i = 0; i < fdb_to_remove->num_entries; i++) names[i] = malloc(128);

        //creo directory handler per scendere nella cartella e cancellare il contenuto ricorsivamente
        DirectoryHandle* directoryHandle_tmp = SimpleFS_directoryHandleAlloc();
        SimpleFS_directoryHandleInit(d->sfs, directoryHandle_tmp, fdb_to_remove->fcb.block_in_disk);

        SimpleFS_readDir(names, directoryHandle_tmp);

        for(i = 0; i < fdb_to_remove->num_entries; i++) {
            SimpleFS_remove(directoryHandle_tmp, names[i]);
        }

        for(i = 0; i < fdb_to_remove->num_entries; i++) free(names[i]);
        free(names);
        free(directoryHandle_tmp);
    }

    // printf("Finito ricorsione\n\n");

    if(d->dcb->num_entries <= MAX_ENTRIES_IN_FDB){
        // printf("Caso solo entries in primo blocco\n\n");
        //just first directory block
        // printf("Da cancellare prima: %d\n\n", d->dcb->file_blocks[index_entry_file.entry]);
        // printf("Da spostare prima: %d\n\n", d->dcb->file_blocks[d->dcb->num_entries - 1]);

        d->dcb->file_blocks[index_entry_file.entry] = d->dcb->file_blocks[d->dcb->num_entries - 1];
        d->dcb->file_blocks[d->dcb->num_entries - 1] = 0;

        // printf("Da cancellare dopo: %d\n\n", d->dcb->file_blocks[index_entry_file.entry]);
        // printf("Da spostare dopo: %d\n\n", d->dcb->file_blocks[d->dcb->num_entries - 1]);

    }
    else{
        //reading the last block of current dir
        index_last_block_current_directory = d->dcb->fcb.index_last_block;
        res = DiskDriver_readBlock(d->sfs->disk, db_last_block_current_directory, index_last_block_current_directory);
        if(res < 0){
            printf("Error while reading last block of current directory %s\n\n", d->dcb->fcb.name);
            exit(-1);
        }

        if((d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB)) n_entries_last_block_current_directory = (d->dcb->num_entries - MAX_ENTRIES_IN_FDB)%(MAX_ENTRIES_IN_DB);
        else n_entries_last_block_current_directory = MAX_ENTRIES_IN_DB;
        // printf("n entries last block of dir %d\n\n", n_entries_last_block_current_directory);

        if(index_entry_file.index_relative_of_block_directory == 0){
            //file si trova nel primo blocco
            // printf("Da cancellare prima: %d\n\n", d->dcb->file_blocks[index_entry_file.entry]);
            // printf("Da spostare prima: %d\n\n", db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1]);
            d->dcb->file_blocks[index_entry_file.entry] = db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1];
            // printf("Da cancellare dopo: %d\n\n", d->dcb->file_blocks[index_entry_file.entry]);

        }
        else if(index_entry_file.index_relative_of_block_directory == (d->dcb->fcb.size_in_blocks - 1)){
            //file si trova nell'ultimo blocco
            // printf("Caso file in ultimo e piu blocchi\n\n");
            // printf("Da cancellare prima: %d\n\n", db_last_block_current_directory->file_blocks[index_entry_file.entry]);
            // printf("Da spostare prima: %d\n\n", db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1]);
            db_last_block_current_directory->file_blocks[index_entry_file.entry] = db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1];
            // printf("Da cancellare dopo: %d\n\n", db_last_block_current_directory->file_blocks[index_entry_file.entry]);

        }
        else{
            // printf("Caso file in mezzo e piu blocchi\n\n");

            DirectoryBlock directoryBlock_middle = {0};
            index_of_block_middle = SimpleFS_findBlockDirectoryByBlockInFile(d, index_entry_file.index_relative_of_block_directory);
            if(index_of_block_middle < 0){
                printf("Error while reading block %d of current directory %s\n\n", index_entry_file.index_relative_of_block_directory, d->dcb->fcb.name);
                exit(-1);
            }
            
            res = DiskDriver_readBlock(d->sfs->disk, &directoryBlock_middle, index_of_block_middle);
            if(res < 0){
                printf("Error while reading middle block of current directory %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }

            // printf("Da cancellare prima: %d\n\n", directoryBlock_middle.file_blocks[index_entry_file.entry]);
            // printf("Da spostare prima: %d\n\n", db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1]);
            directoryBlock_middle.file_blocks[index_entry_file.entry] = db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1];
            // printf("Da cancellare dopo: %d\n\n", directoryBlock_middle.file_blocks[index_entry_file.entry]);

            res = DiskDriver_writeBlock(d->sfs->disk, &directoryBlock_middle, index_of_block_middle);
            if(res < 0){
                printf("Error while uploading middle block of current directory %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }
        }

        db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1] = 0;
        // printf("Da spostare dopo: %d\n\n", db_last_block_current_directory->file_blocks[n_entries_last_block_current_directory - 1]);


        if(n_entries_last_block_current_directory - 1 == 0){
            //devo eliminare ultimo blocco current directory e diminuire size in block
            // printf("Sto eliminando\n\n");

            res = SimpleFS_reduceCurrentDirectory(d, (FirstDirectoryBlock*)db_last_block_current_directory, index_last_block_current_directory);
            if(res < 0){
                printf("Error while deleting last block of current directory %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }

            res = DiskDriver_freeBlock(d->sfs->disk, index_last_block_current_directory);
            if(res < 0){
                printf("Error while freeing last block of current directory %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }

            d->dcb->fcb.size_in_blocks -= 1;
        }
        else{
            // printf("Salvo perchè non elimino\n\n");

            res = DiskDriver_writeBlock(d->sfs->disk, db_last_block_current_directory, index_last_block_current_directory);
            if(res < 0){
                printf("Error while updating last block of current directory %s\n\n", d->dcb->fcb.name);
                exit(-1);
            }
        }
    }

    d->dcb->num_entries -= 1;

    //salvo modifiche fatte a directoryHandle
    res = DiskDriver_writeBlock(d->sfs->disk, d->dcb, d->dcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error when updating first block of current directory %s\n\n", d->dcb->fcb.name);
        return -1;
    }
    
    //libero tutti i blocchi che occupa un file
    SimpleFS_freeAllBlocksOfFile(d, &ffb_to_remove);

    // printf("MAX FFB: %lu, MAX FB: %lu\n\n", MAX_BYTE_IN_FFB, MAX_BYTE_IN_FB);
    return 0;
}

void SimpleFS_saveDirectoryHandleOnDisk(DirectoryHandle* d){
    int res;

    res = DiskDriver_writeBlock(d->sfs->disk, d->dcb, d->dcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error while updating on disk fdb of current dir %s\n\n", d->dcb->fcb.name);
        exit(-1);
    }
}

int SimpleFS_syncCurrDir(DirectoryHandle* d){
    int res;

    res = DiskDriver_readBlock(d->sfs->disk, d->dcb, d->dcb->fcb.block_in_disk);
    if(res < 0){
        printf("Error while reading directory to sync\n\n");
        return -1;
    }
    
    if(d->p_directory != NULL){
        res = DiskDriver_readBlock(d->sfs->disk, d->p_directory, d->p_directory->fcb.block_in_disk);
        if(res < 0){
            printf("Error while reading parent directory to sync\n\n");
            return -1;
        }
    }
    
    return 0;
}

void printDir(DirectoryHandle* d){
    printf("Situation of dir '%s' -->\n", d->dcb->fcb.name);
    printf(" FS Address: %p\n FDB Address: %p\n FDB_P: %p\n CURRENT_BLOCK: %p\n POS_IN_DIR: %d\n POS_IN_BLOCK: %d\n",
            d->sfs, d->dcb, d->p_directory, d->current_block, d->pos_in_dir, d->pos_in_block);
    printf(" DISK Address: %p\n IDX_ROOT: %d\n IDX_CURR_DIR: %d\n",
            d->sfs->disk, d->sfs->root, d->sfs->curr_dir);
    printf(" NAME: %s\n IDX_FDB_PARENT: %d\n BLOCK_IN_DISK: %d\n SIZE_IN_BYTES: %d\n SIZE_IN_BLOCKS: %d\n IS_DIR: %d\n",
            d->dcb->fcb.name, d->dcb->fcb.directory_block, d->dcb->fcb.block_in_disk, d->dcb->fcb.size_in_bytes, d->dcb->fcb.size_in_blocks, d->dcb->fcb.is_dir);
    printf(" IDX_PREVIOUS: %d\n IDX_NEXT: %d\n BLOCK_IN_FILE(0 IF FCB): %d\n",
            d->dcb->header.previous_block, d->dcb->header.next_block, d->dcb->header.block_in_file);
    printf(" NUM_ENTRIES_TOT: %d\n", d->dcb->num_entries);
    printf(" INDEX OF LAST BLOCK: %d\n", d->dcb->fcb.index_last_block);

    if(d->dcb->num_entries > 0){
        printf("\nFile in fdb of dir -->\n");

        int idx_block_file, res, cnt = 0;
        int idx_max_fb = (BLOCK_SIZE
            -sizeof(BlockHeader)
            -sizeof(FileControlBlock)
                -sizeof(int))/sizeof(int); //87
        
        for(; cnt < idx_max_fb; cnt++){
            idx_block_file = d->dcb->file_blocks[cnt];
            if(idx_block_file == 0) break;
            printf(" File in entry n. %d, in idx_block_of_file: %d\n", cnt, idx_block_file);
        }

        char read_block_dir_tmp[BLOCK_SIZE] = {0};
        idx_max_fb = (BLOCK_SIZE-sizeof(BlockHeader))/sizeof(int);

        while(cnt < d->dcb->num_entries){
            printf("\n File in block n. %d -->\n", d->current_block->next_block);
            res = DiskDriver_readBlock(d->sfs->disk, read_block_dir_tmp, d->current_block->next_block);
            if(res < 0){
                printf("Error in read: SimpleFs_readDir 2\n\n");
                exit(-1);
            }
            DirectoryBlock* db_tmp = (DirectoryBlock*)read_block_dir_tmp;
            d->current_block = &(db_tmp->header);
            
            for(int i=0; i < idx_max_fb; i++){
                idx_block_file = db_tmp->file_blocks[i];
                if(idx_block_file == 0) break;
                printf(" File in entry n. %d, in idx_block_of_file: %d\n", i, idx_block_file);
                cnt++;
            }

            printf("\n IDX_PREVIOUS: %d\n IDX_NEXT: %d\n BLOCK_IN_FILE(0 IF FCB): %d\n",
                    db_tmp->header.previous_block, db_tmp->header.next_block, db_tmp->header.block_in_file);
        }
    }
    d->current_block = &(d->dcb->header);
}

void printFile(FileHandle* f){
    printf("Situation of file: '%s'-->\n", f->fcb->fcb.name);
    printf(" FS Address: %p\n FFB Address: %p\n FDB_P: %p\n CURRENT_BLOCK: %p\n POS_IN_FILE: %d\n",
            f->sfs, f->fcb, f->p_directory, f->current_block, f->pos_in_file);
    printf(" DISK Address: %p\n IDX_ROOT: %d\n IDX_CURR_DIR: %d\n",
            f->sfs->disk, f->sfs->root, f->sfs->curr_dir);
    printf(" NAME: %s\n IDX_FDB_PARENT: %d\n BLOCK_IN_DISK: %d\n SIZE_IN_BYTES: %d\n SIZE_IN_BLOCKS: %d\n IS_DIR: %d\n",
            f->fcb->fcb.name, f->fcb->fcb.directory_block, f->fcb->fcb.block_in_disk, f->fcb->fcb.size_in_bytes, f->fcb->fcb.size_in_blocks, f->fcb->fcb.is_dir);
    printf(" IDX_PREVIOUS: %d\n IDX_NEXT: %d\n BLOCK_IN_FILE(0 IF FCB): %d\n",
            f->fcb->header.previous_block, f->fcb->header.next_block, f->fcb->header.block_in_file);
    printf(" INDEX OF LAST BLOCK: %d\n", f->fcb->fcb.index_last_block);    

    if(f->fcb->fcb.size_in_bytes == 0){
        printf("File is empty!\n\n");
        return;
    }
    int saved_prev_pos = f->pos_in_file;
    SimpleFS_seek(f, 0);

    char buf1[MAX_BYTE_IN_FFB + 1];
    memset(buf1, 0, MAX_BYTE_IN_FFB + 1);
    char bufelse[MAX_BYTE_IN_FB + 1];
    memset(bufelse, 0, MAX_BYTE_IN_FB + 1);


    SimpleFS_read(f, buf1, MAX_BYTE_IN_FFB);
    printf("\nData in ffb of file -->\n");
    printf("%s\n", buf1);

    char tmp[BLOCK_SIZE] = {0};
    for(int i=0; i<f->fcb->fcb.size_in_blocks - 1; i++){
        printf("\nData and situation of block n. %d -->\n", i+1);
        int next = f->current_block->next_block;
        DiskDriver_readBlock(f->sfs->disk, tmp, next);
        FileBlock* p_tmp = (FileBlock*)tmp;
        memcpy(bufelse, p_tmp->data, BLOCK_SIZE  - sizeof(BlockHeader));
        printf(" PREVIOUS_IDX: %d\n NEXT_IDX: %d\n BLOCK_IN_FILE: %d\n", p_tmp->header.previous_block, p_tmp->header.next_block, p_tmp->header.block_in_file);
        printf("%s\n", bufelse);
        f->current_block = &(p_tmp->header);
    }
    f->pos_in_file = saved_prev_pos;
    f->current_block = &(f->fcb->header);
}

void SimpleFS_tree_aux(DirectoryHandle* d, int level){
    int res;

    for(int i = 0; i < level; i++){
        if(i == level-1) printf("|----- ");
        else{
            printf("|      ");
        }
    }
    if(strcmp("/", d->dcb->fcb.name) == 0) printf("%s\n", d->dcb->fcb.name);
    else printf("%s/\n", d->dcb->fcb.name);

    level++;

    int num_entries = d->dcb->num_entries;
    if(num_entries == 0) return;

    char** names = (char**)malloc(sizeof(char*)*num_entries);
    for(int i = 0; i < num_entries; i++) names[i] = (char*)malloc(128);  

    SimpleFS_readDir(names, d);

    for(int i = 0; i < num_entries; i++){
        res = SimpleFS_changeDir(d, names[i]);
        if(res < 0) {
            for(int j = 0; j < level; j++){
                if(j == level-1) printf("|----- ");
                else{
                    printf("|      ");
                }
            } 
            printf("%s\n", names[i]);
        }
        else{
            SimpleFS_tree_aux(d, level);
            res = SimpleFS_changeDir(d, "..");
        }
    } 

    for(int i=0; i<num_entries; i++) free(names[i]);
    free(names);
}

void SimpleFS_tree(SimpleFS* fs){
    DirectoryHandle* directoryHandle_tmp = SimpleFS_directoryHandleAlloc();
    SimpleFS_directoryHandleInit(fs, directoryHandle_tmp, fs->root);

    printf("Directories Tree -->\n\n");
    
    SimpleFS_tree_aux(directoryHandle_tmp, 0);

    free(directoryHandle_tmp->p_directory);
    free(directoryHandle_tmp->dcb);
    free(directoryHandle_tmp);
}

static char *rand_string(char *str, size_t size){
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJK...";
    if (size) {
        --size;
        for (size_t n = 0; n < size; n++) {
            int key = rand() % (int) (sizeof charset - 1);
            str[n] = charset[key];
        }
        str[size] = '\0';
    }
    return str;
}

char* rand_string_alloc(size_t size){
    char *s = malloc(size + 1);
    if (s) rand_string(s, size);
    return s;
}

void BeEvilWhenTestingSimpleFS(SimpleFS* fs,DiskDriver* disk, const char* name_of_disk, int num_of_blocks){   
    int res;

    DiskDriver_init(disk, name_of_disk, num_of_blocks);

    printf("Testing on SimpleFS...\n\n");
    
    // printf("Situazione prima della formattazione:\n");
    // printDisk(disk);

    DirectoryHandle* d = SimpleFS_init(fs, disk);

    printf("Situazione:\n");
    printDisk(disk);

    SimpleFS_mkDir(d, "AeukeqwqBfKtshmCmtjqkwBgvHmbpt.bytluBzChEysaywDKbEemmGkzzwtacsHsEeEjvtqmJIetjHqkpuwteHfvhynbIgtyccA");

    SimpleFS_changeDir(d, "AeukeqwqBfKtshmCmtjqkwBgvHmbpt.bytluBzChEysaywDKbEemmGkzzwtacsHsEeEjvtqmJIetjHqkpuwteHfvhynbIgtyccA");

    printf("Situazione directory AeukeqwqBfKtshmCmtjqkwBgvHmbpt.bytluBzChEysaywDKbEemmGkzzwtacsHsEeEjvtqmJIetjHqkpuwteHfvhynbIgtyccA:\n\n");
    printDir(d);

    char* string = NULL;
    for(int i = 0; i < 2000; i++){
        string = rand_string_alloc(120);
        res = SimpleFS_mkDir(d, string);
        free(string);
        if(res < 0) printf("Error while testing %d\n\n", i);
    }
    
    string = rand_string_alloc(120);
    res = SimpleFS_mkDir(d, string);
    if(res < 0) printf("Error while testing manually creation of dir\n\n");

    printf("Change dir in %s\n\n", string);
    SimpleFS_changeDir(d, string);

    SimpleFS_createFile(d, "my_file");

    // free(string);

    printDisk(disk);

    SimpleFS_tree(fs);

    // char** names = (char**)malloc(sizeof(char*)*d->dcb->num_entries);
    // for(int i = 0; i < d->dcb->num_entries; i++) names[i] = (char*)malloc(128);

    // res = SimpleFS_readDir(names, d);
    // if(res < 0) printf("Error while testing read Dir\n\n");

    // printf("Directory %s contains %d entries\n\n", d->dcb->fcb.name, res);

    printDir(d);

    // for(int i = 0; i < d->dcb->num_entries; i++) free(names[i]);
    // free(names);

    free(d->p_directory);
    free(d->dcb);
    free(d);

    DiskDriver_flush(disk);
}
