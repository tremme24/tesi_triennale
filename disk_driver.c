#include "disk_driver.h"

void DiskDriver_init(DiskDriver* disk, const char* filename, int num_blocks){
    
    if(!disk){
        printf("Insert a valid disk struct\n\n");
        exit(-1);
    }

    if(strcmp(filename, "") == 0){
        printf("Insert a valid name for the disk\n\n");
        exit(-1);
    }

    int bitmap_entries = num_blocks%8? (num_blocks/8)+1 : (num_blocks/8);  // how many bytes are needed to store the bitmap
    int more_space_needed_bitmap = bitmap_entries - (BLOCK_SIZE - sizeof(DiskHeader));
    int bitmap_blocks = more_space_needed_bitmap>0? (more_space_needed_bitmap/BLOCK_SIZE)+2 : 1;   // how many blocks to store the bitmap on disk
    int filesize = num_blocks*BLOCK_SIZE;

    int fd = open(filename, O_CREAT | O_RDWR | O_EXCL, 0777);
    if(fd < 0){
        
        if(errno == EEXIST){
            fd = open(filename, O_RDWR, 0777);
            DiskDriver_fillDiskDriverStruct(disk, fd, bitmap_blocks);
            printf("Disk already created --> Just opening it\n\n");
            return;
        }
        else{
            perror("Error when opening the file\n");
            exit(-1);
        }
    }
    else{

        int res = DiskDriver_stretchFile(fd, filesize);
        if(res){
            printf("Error while stretching file\n");
            exit(-1);
        }
        
        DiskDriver_fillDiskDriverStruct(disk, fd, bitmap_blocks);

        disk->header->num_blocks = num_blocks;
        disk->header->bitmap_blocks = bitmap_blocks;
        disk->header->bitmap_entries = bitmap_entries;
        disk->header->free_blocks = num_blocks - bitmap_blocks;
        disk->header->first_free_block = bitmap_blocks;
        
        BitMap bitmap_temp = BitMap_init(num_blocks, disk->bitmap_data);
        
        for(int i = 0; i < bitmap_blocks; i++){
            BitMap_set(&bitmap_temp, i, 1);
        }
        
        DiskDriver_flush(disk);
    }
}

int DiskDriver_stretchFile(int fd, int filesize){
    int res = lseek(fd, filesize-1, SEEK_SET);
    if(res != filesize-1){
        return -1;
    }

    res = write(fd, "" , 1);
    if(!res){
        return -1;
    }

    res = lseek(fd, 0, SEEK_SET);
    if(res != 0){
        return -1;
    }
    
    return 0;
}

void DiskDriver_fillDiskDriverStruct(DiskDriver* disk, int fd, int bitmap_blocks){
    disk->fd = fd;
    char* mmapped_area = mmap(NULL, (bitmap_blocks)*BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    disk->bitmap_data = mmapped_area + sizeof(DiskHeader);
    disk->header = (DiskHeader*)mmapped_area;
}

int DiskDriver_readBlock(DiskDriver* disk, void* dest, int block_num){
    if(block_num >= disk->header->num_blocks || block_num < 0 || block_num < disk->header->bitmap_blocks) {
        return -1;
    }

    BitMapEntryKey bitmapEntryKey = BitMap_blockToIndex(block_num);
    unsigned char bit = BitMap_isBusy(&bitmapEntryKey, disk->bitmap_data);

    if(bit == 0) {
        return -1;
    }

    if(pread(disk->fd, dest, BLOCK_SIZE, block_num*BLOCK_SIZE) < 0){
        return -1;
    } 

    return 0;
}

int DiskDriver_writeBlock(DiskDriver* disk, void* src, int block_num){
    if(block_num >= disk->header->num_blocks || block_num < 0 || block_num < disk->header->bitmap_blocks) {
        return -1;
    }

    // BitMapEntryKey bitmapEntryKey = BitMap_blockToIndex(block_num);
    // unsigned char bit = BitMap_isBusy(&bitmapEntryKey, disk->bitmap_data);

    // if(bit == 1) {
    //     return -1;
    // }  

    if(pwrite(disk->fd, src, BLOCK_SIZE, block_num*BLOCK_SIZE) < 0){
        return -1;
    } 

    BitMapEntryKey bitmapEntryKey = BitMap_blockToIndex(block_num);
    unsigned char bit = BitMap_isBusy(&bitmapEntryKey, disk->bitmap_data);

    if(!bit){
        BitMap bitmap_temp = BitMap_init(disk->header->num_blocks, disk->bitmap_data);
        // bitmap_temp.entries = disk->bitmap_data;
        // bitmap_temp.num_bits = disk->header->num_blocks;
        BitMap_set(&bitmap_temp, block_num, 1);

        disk->header->free_blocks -= 1;
        //if(--disk->header->free_blocks == 0) disk->header->first_free_block = -1;
        if(block_num == disk->header->first_free_block) disk->header->first_free_block = DiskDriver_getFreeBlock(disk, block_num);
    }

    return 0;
}

int DiskDriver_freeBlock(DiskDriver* disk, int block_num){
    if(block_num >= disk->header->num_blocks || block_num < 0 || block_num < disk->header->bitmap_blocks) {
        return -1;
    }
    // if(block_num < disk->header->bitmap_blocks){
    //     return -1;
    // }
    
    BitMapEntryKey bitmapEntryKey = BitMap_blockToIndex(block_num);
    unsigned char bit = BitMap_isBusy(&bitmapEntryKey, disk->bitmap_data);

    if(!bit) return 0;

    else{
        // unsigned char reset_data_buffer[BLOCK_SIZE] = {0};
        // memset(reset_data_buffer, 0, BLOCK_SIZE);

        // if(pwrite(disk->fd, reset_data_buffer, BLOCK_SIZE, block_num*BLOCK_SIZE) < 0){
        //     return -1;
        // }

        BitMap bitmap_temp = BitMap_init(disk->header->num_blocks, disk->bitmap_data);
        // temp.entries = disk->bitmap_data;
        // temp.num_bits = disk->header->num_blocks;
        BitMap_set(&bitmap_temp, block_num, 0);

        disk->header->free_blocks += 1;

        if(disk->header->first_free_block < 0 || block_num < disk->header->first_free_block) disk->header->first_free_block = block_num;

        return 0; 
    }
}

int DiskDriver_getFreeBlock(DiskDriver* disk, int start){
    BitMap bitmap_temp = BitMap_init(disk->header->num_blocks, disk->bitmap_data);
    // bitmap_temp.entries = disk->bitmap_data;
    // bitmap_temp.num_bits = disk->header->num_blocks;

    return BitMap_get(&bitmap_temp, start, 0);
}

int DiskDriver_flush(DiskDriver* disk){
    int res = msync((char*)disk->header, (disk->header->bitmap_blocks)*BLOCK_SIZE , MS_SYNC);
    if(res < 0){
        perror("Error flushing mmap'd data\n");
        exit(-1);
    }
    return res;
}

void BeEvilWhenTestingDiskDriver(DiskDriver* disk, const char* filename, int num_blocks){

    int res;
    //BitMap bitmap_debug_struct;

    DiskDriver_init(disk, filename, num_blocks);
    //bitmap_debug_struct = BitMap_init(disk->header->num_blocks, disk->bitmap_data);

    printf("Testing on Disk Driver...\n\n");
    
    printDisk(disk);
    
    printf("\n\nTesting on Write/Read Block...\n\n");

    unsigned char dest[BLOCK_SIZE+1] = {0};
    unsigned char src[BLOCK_SIZE] = {0};
    memset(src, 65, BLOCK_SIZE);

    //Worst cases read block
    res = DiskDriver_readBlock(disk, dest, -4);
    if(res < 0) printf("Errore ReadBlock: idx_block = -4\n\n");
    else printf("Lettura riuscita: idx_block = -4\ndest = %s\n\n", dest);
    
    res = DiskDriver_readBlock(disk, dest, 20000);
    if(res < 0) printf("Errore ReadBlock: idx_block = 20000\n\n");
    else printf("Lettura riuscita: idx_block = 20000\ndest = %s\n\n", dest);

    res = DiskDriver_readBlock(disk, dest, 199);
    if(res < 0) printf("Errore ReadBlock: idx_block = 199\n\n");
    else printf("Lettura riuscita: idx_block = 199\ndest = %s\n\n", dest);

    res = DiskDriver_readBlock(disk, dest, 1);
    if(res < 0) printf("Errore ReadBlock: idx_block = 1\n\n");
    else printf("Lettura riuscita: idx_block = 1\ndest = %s\n\n", dest);

    res = DiskDriver_readBlock(disk, dest, 0);
    if(res < 0) printf("Errore ReadBlock: idx_block = 0\n\n");
    else printf("Lettura riuscita: idx_block = 0\ndest = %s\n\n", dest);

    printDisk(disk);
    // debugBmap(&bitmap_debug_struct);
    printf("\n\n");

    //Worst cases write block
    res = DiskDriver_writeBlock(disk, src, -4);
    if(res < 0) printf("Errore WriteBlock: idx_block = -4\n\n");
    else {
        printf("Scrittura riuscita: idx_block = -4\n");
        res = DiskDriver_readBlock(disk, dest, -4);
        printf("%s\n\n", dest);
    }

    res = DiskDriver_writeBlock(disk, src, 20000);
    if(res < 0) printf("Errore WriteBlock: idx_block = 20000\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 20000\n");
        res = DiskDriver_readBlock(disk, dest, 20000);
        printf("%s\n\n", dest);
    }
    
    res = DiskDriver_writeBlock(disk, src, 1);
    if(res < 0) printf("Errore WriteBlock: idx_block = 1\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 1\n");
        res = DiskDriver_readBlock(disk, dest, 1);
        printf("%s\n\n", dest);
    }
    
    res = DiskDriver_writeBlock(disk, src, 6);
    if(res < 0) printf("Errore WriteBlock: idx_block = 6\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 6\n");
        res = DiskDriver_readBlock(disk, dest, 6);
        printf("stringa: %s\n\n", dest);
    }

    res = DiskDriver_writeBlock(disk, src, 3);
    if(res < 0) printf("Errore WriteBlock: idx_block = 3\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 3\n");
        res = DiskDriver_readBlock(disk, dest, 3);
        printf("stringa: %s\n\n", dest);
    }

    memset(src, 66, BLOCK_SIZE);

    res = DiskDriver_writeBlock(disk, src, 6);
    if(res < 0) printf("Errore WriteBlock: idx_block = 6\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 6\n");
        res = DiskDriver_readBlock(disk, dest, 6);
        printf("stringa: %s\n\n", dest);
    }

    res = DiskDriver_writeBlock(disk, src, 0);
    if(res < 0) printf("Errore WriteBlock: idx_block = 0\n\n");
    else {
        printf("Scrittura riuscita: idx_block = 0\n");
        res = DiskDriver_readBlock(disk, dest, 0);
        printf("stringa: %s\n\n", dest);
    }

    printDisk(disk);
    // debugBmap(&bitmap_debug_struct);
    printf("\n\n");

    for(int i = 0; i < 200; i++){
        res = DiskDriver_writeBlock(disk, src, i);
        if(res < 0) printf("Errore WriteBlock: idx_block = %d\n\n", i);
        else {
            printf("Scrittura riuscita: idx_block = %d\n", i);
            printf("First free block = %d\n", disk->header->first_free_block);
        }
    }

    printDisk(disk);
    // debugBmap(&bitmap_debug_struct);
    printf("\n\n");

    for(int i = 200; i >= 0; i--){
        res = DiskDriver_freeBlock(disk, i);
        if(res < 0) printf("Errore FreeBlock: idx_block = %d\n\n", i);
        else {
            printf("Liberazione eseguita: idx_block = %d\n", i);
            printf("First free block = %d\n", disk->header->first_free_block);
        }
    }

    printDisk(disk);
    // debugBmap(&bitmap_debug_struct);
    printf("\n\n");

    for(int i = 0; i < 200; i++){
        res = DiskDriver_freeBlock(disk, i);
        if(res < 0) printf("Errore FreeBlock: idx_block = %d\n\n", i);
        else {
            printf("Liberazione eseguita: idx_block = %d\n", i);
            printf("First free block = %d\n", disk->header->first_free_block);
        }
    }

    printDisk(disk);
    // debugBmap(&bitmap_debug_struct);
    printf("\n\n");

    for(int i = 0; i < 200; i++){
        res = DiskDriver_writeBlock(disk, src, i);
        if(res < 0) printf("Errore WriteBlock: idx_block = %d\n\n", i);
        else {
            printf("Scrittura riuscita: idx_block = %d\n", i);
            printf("First free block = %d\n", disk->header->first_free_block);
        }
    }

    res = DiskDriver_freeBlock(disk, 100);
    if(res < 0) printf("Errore FreeBlock: idx_block = %d\n\n", 100);
    else {
        printf("Liberazione eseguita: idx_block = %d\n", 100);
        printf("First free block = %d\n", disk->header->first_free_block);
    }

    printDisk(disk);
    // debugBmap(&bitmap_debug_struct);
    printf("\n\n");

    res = DiskDriver_flush(disk);
    if(res < 0) printf("Error in DiskDriver_flush\n\n");
    else{
        printf("Flush successful\n");
    }
}

void printDisk(DiskDriver* disk){
    printf("Situation of disk -->\n");
    printf(" DISKHEADER: %p\n BITMAP: %p\n FD: %d\n",
            disk->header, disk->bitmap_data, disk->fd);
    printf(" NUM_BLOCKS: %d\n BITMAP_BLOCKS: %d\n BITMAP_ENTRIES: %d\n FREE_BLOCKS: %d\n FIRST_FREE_BLOCK: %d\n",
            disk->header->num_blocks, disk->header->bitmap_blocks, disk->header->bitmap_entries, disk->header->free_blocks, disk->header->first_free_block);
}