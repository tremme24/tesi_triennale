# FAP32 (:D)

A university project for [Operating System's](https://gitlab.com/grisetti/sistemi_operativi_2018_19) course aiming on building a File System from a given ['template'](https://gitlab.com/grisetti/sistemi_operativi_2018_19/tree/master/projects/simple_file_system).

#

## Getting Started

These are instruction that will let you compile and run this filesystem in a Unix environment.  
This filesystem has a purely didactic use, and no real application.

### Prerequisites

In order to use this FS you need a C compiler (gcc / clang)

To install them type the correct istruction in the terminal, depending on your OS:

***Ubuntu-based:***
```
sudo apt-get update
sudo apt-get install build-essential
```
***Darwin(MacOSX):***
```
xcode-select --install
```

#
### Running

A step by step series of examples that tell you how to get a development env running

*Run the makefile in the terminal to compile the executable:*


```
make
```

then type

```
make run
```

to run a terminal based test of the **FS**

## Running the tests

To run the tests you need to type the desidered size of your "disk" and press enter then type your choice between several commands in the terminal and press enter.

### What can you do?

Every test is aimed to simulate the workload of a regular filesystem, being creating a file or folder, navigate and opening them and so on, all stored in an extension-less file.

*For example:*

```
1. createfile: followed by the name of file 
```
means that by typing *createfile* and a name you'll create an empty file named how you desired

#

## Documentation

Please read [DOCUMENTATION.md](https://gitlab.com/alessioprescenzo/progettoso2/blob/master/DOCUMENTATION.md) for a detailed analysis on the code of our work 

### Written with
* The entire **FS** is written in **C**
* README.md and DOCUMENTATION.md written in **Markdown**


#

## Authors

* **Marco Mormando** - *Developer* - [tremme24](https://gitlab.com/tremme24)
* **Alessio Prescenzo** - *Developer* - [alessioprescenzo](https://gitlab.com/alessioprescenzo)

## Acknowledgments

* Thanks to our [professor](https://gitlab.com/grisetti) for the template and suggestions


